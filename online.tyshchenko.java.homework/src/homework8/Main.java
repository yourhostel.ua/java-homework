package homework8;

import homework8.controller.FamilyController;
import homework8.dao.CollectionFamilyDao;
import homework8.model.baseClasses.Family;
import homework8.model.baseClasses.Human;
import homework8.model.derivativesFromHuman.Man;
import homework8.model.derivativesFromHuman.Woman;
import homework8.model.derivativesFromPet.Dog;
import homework8.model.derivativesFromPet.DomesticCat;
import homework8.model.derivativesFromPet.Fish;
import homework8.model.derivativesFromPet.RoboCat;
import homework8.model.enumerations.Species;
import homework8.service.FamilyService;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import static homework8.model.enumerations.DayOfWeek.*;
import static homework8.helpers.Helpers.allChilds;
import static libs.Console.f;
import static libs.Console.log;

public class Main {
    public static final String GREEN = "\u001B[32m";
    public static final String CYAN = "\u001B[36m";
    public static final String RESET = "\u001B[0m";
    public static final String YELLOW = "\u001B[33m";

    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        log("totalMemory: " + runtime.totalMemory() / 1_000_000 + " Mbyte\n");
        log("freeMemory: " + runtime.freeMemory() / 1_000_000 + " Mbyte\n");
        for (int i = 0; i < 10; i++) {//у мене запускався гарбідж колектор з мільйона
            new Human();
        }
        //System.gc();

        log(CYAN + "create dogGlisy -------------------------------------------------------------------\n" + RESET);
        Dog dogGlisy = new Dog(3, 60, Set.of(new String[]{"eat", "drink", "sleep"}), Species.DOG, "Glisy");
        dogGlisy.foul();
        dogGlisy.eat();
        dogGlisy.respond();
        log(dogGlisy + "\n");//toString()

        Man JuliusCaesar = new Man(60, 100, Map.of(SUNDAY.name(), "a", MONDAY.name(), "b", TUESDAY.name(), "c", WEDNESDAY.name(), "d"), "Caesar", "Julius");

        Woman CorneliaCinna = new Woman(28, 100, Map.of(SUNDAY.name(), "task", MONDAY.name(), "task_2"), "Cornelia", "Cinna");

        Human JuliaCinna = new Woman(10, "Julia", "Caesar");

        Human AugustusCaesar = new Man(12, "Augustus", "Caesar");
        AugustusCaesar.setYear(13);

        Human JessePinkman = new Man(27, "Jesse", "Pinkman");

        log(CYAN + "Family Caesar addChilds -----------------------------------------------------------\n" + RESET);
        Family familyCaesar = new Family(CorneliaCinna, JuliusCaesar, "Caesar");
        familyCaesar.setPet(dogGlisy);
        familyCaesar.addChild(JuliaCinna);
        familyCaesar.addChild(AugustusCaesar);
        log(familyCaesar + "\n");//toString()


        log(CYAN + "Julius Caesar ---------------------------------------------------------------------\n" + RESET);
        JuliusCaesar.feedPet();//Кормлю нашего питомца собака Glisy - викликаний на family
        log(JuliusCaesar + "\n");//toString()
        JuliusCaesar.greetPet();
        JuliusCaesar.describePet();
        log(CYAN + "Cornelia Cinna --------------------------------------------------------------------\n" + RESET);
        log(CorneliaCinna + "\n");//toString()
        log(CYAN + "Family Caesar children removed ----------------------------------------------------\n" + RESET);

        familyCaesar.setPet(new DomesticCat(3, 45, Set.of("eat"), Species.CAT, "Барсик"));
        familyCaesar.getPet(0).setHabits("the cat likes to tear the chair");

        familyCaesar.deleteChild(JessePinkman);//ребенка Jesse Pinkman в этой семье нет
        familyCaesar.deleteChild(AugustusCaesar);//ребенок Augustus Caesar удален из семьи Caesar
        familyCaesar.deleteChild(1);//в семье Caesar детей меньше чем задан индекс
        familyCaesar.deleteChild(0);//ребенок Julia Caesar удален из семьи Caesar
        familyCaesar.deleteChild(JessePinkman);//в семье Caesar детей нет
        log(familyCaesar + "\n");//toString()

        log(CYAN + "JuliusCaesar.getFamily().deleteFamily() -------------------------------------------\n" + RESET);
        JuliusCaesar.getFamily().deleteFamily();
        log(CYAN + "Надаємо запит на сім'ю familyCaesar -----------------------------------------------\n" + RESET);
        log(familyCaesar + "\n");//toString()//викличе помилку - Ошибка запроса данных о семье
        log(CYAN + "Пробуємо повернути сім'ю на JuliusCaesar ------------------------------------------\n" + RESET);
        log("JuliusCaesar.getFamily() = " + JuliusCaesar.getFamily() + "\n");

        log(CYAN + "Створюємо тестові сім'ї -----------------------------------------------------------\n" + RESET);
        log(CYAN + "сім'я StritskyBiel ----------------------------------------------------------------\n" + RESET);
        Man BobStritsky = new Man(27, "Bob", "Stritsky");
        Woman JessikaBiel = new Woman(27, 86, Map.of(SUNDAY.name(), "Do home work", MONDAY.name(), "Go to courses", TUESDAY.name(), "Go to explore the mountains"), "Jessika", "Biel");
        Family StritskyBiel = new Family(JessikaBiel, BobStritsky, "StritskyBiel");
        log(StritskyBiel + "\n");//toString()

        log(String.format("%s %s\n", BobStritsky.getName(), BobStritsky.repairCar()));//Bob чинит авто
        log(String.format("%s %s\n", JessikaBiel.getName(), JessikaBiel.makeup()));//Jessika краситься

        log(CYAN + "сім'я Besson ----------------------------------------------------------------------\n" + RESET);
        Human LucBesson = new Human();
        LucBesson.setYear(64);
        LucBesson.setName("Luc");
        LucBesson.setSurname("Besson");

        Human AnneParillaud = new Human(62, 90, Map.of("day1", "task1"), "Anne", "Parillaud");
        Family Besson = new Family(AnneParillaud, LucBesson, "Besson");

        Human GiuliettaBesson = new Human(36, "Giulietta", "Besson");
        Besson.addChild(GiuliettaBesson);
        log(Besson + "\n");//toString()
        log(CYAN + "TrickyPets ------------------------------------------------------------------------\n" + RESET);
        Dog Julya = new Dog(Species.DOG, "Julya");
        Julya.foul();
        DomesticCat Sonya = new DomesticCat(Species.DOMESTIC_CAT, "Sonya");
        Sonya.foul();
        Sonya.eat();
        log(CYAN + "OtherPets -------------------------------------------------------------------------\n" + RESET);
        Fish Lester = new Fish(Species.FISH, "Lester");
        //Lester.foul();//помилка тому що не імплементує інтерфейс хитрої тварини
        Lester.respond();//Lester - это рыба
        Lester.eat();//Lester кушает
        RoboCat MarsCat = new RoboCat(Species.ROBO_CAT, "MarsCat");
        //MarsCat.foul();//помилка тому що не імплементує інтерфейс хитрої тварини
        MarsCat.respond();
        MarsCat.eat();
        RoboCat robo = new RoboCat("MarsCat"); //Species.UNKNOWN
        Besson.setPet(robo);
        log(Besson + "\n");//pet = UNKNOWN

        log(YELLOW + "DAO -----------------------------------------------------------------------------\n" + RESET);
        FamilyService s = new FamilyService(new CollectionFamilyDao(new ArrayList<>()));
        FamilyController c = new FamilyController(s);
        //создаем несколько семей
        //Williams-Smith
        Human Oliver = new Human(52, "Oliver", "Williams");
        Human Anna = new Human(47, "Anna", "Smith");
        c.service.createNewFamily(Oliver, Anna, "Williams-Smith");
        c.service.addPet(0, new Dog(5, 60, Set.of(new String[]{"house training issues", "jumping up", "pulling on the leash"}), Species.DOG, "Daisy"));
        c.service.addPet(0, new Fish("Lester"));
        Family Williams_Smith = c.service.bornChild(c.service.getFamilyById(0), "Jack", "Mary");
        //Brown
        Human George = new Human(49, "George", "Brown");
        Human Victoria = new Human(48, "Anna", "Miller");
        Family Brown = new Family(George, Victoria, "Brown");
        Human Sophia = new Human(9, "Sophia", "Brown");
        Brown.addChild(Sophia);
        Human George_Jr = new Human(7, "George.Jr", "Brown");
        Brown.addChild(George_Jr);
        Human Diana_Jr = new Human(3, "Diana.Jr", "Brown");
        Brown.addChild(Diana_Jr);
        c.service.addFamily(Brown);

        Besson = c.service.addFamily(Besson);
        StritskyBiel = c.service.addFamily(StritskyBiel);
        Brown = c.service.adoptChild(Brown, new Human(10, "Charley", "Chaplin"));

        c.service.displayAllFamilies();
        f(GREEN + "длинна списка семей - %d\n" + RESET, c.service.getAllFamilies().size());
        log(GREEN + "Все семьи кол-во человек в которых больше чем 5:\n" + RESET);
        c.service.getFamiliesBiggerThan(5);
        log(GREEN + "Все семьи кол-во человек в которых меньше чем 3:\n" + RESET);
        c.service.getFamiliesLessThan(3);
        log(GREEN + "Все семьи кол-во человек в которых равно 2:\n" + RESET);
        c.service.countFamiliesWithMemberNumber(2);
        log(GREEN + "кол-во семей: " + RESET);
        log(c.service.count() + "\n");
        log(GREEN + "У семьи по индексу 0 такие животные:\n" + RESET);
        log(c.service.getPets(0) + "\n");
        log(YELLOW + "---------------------------------------------------------------------------------\n" + RESET);
        log(GREEN + "Удалить всех детей из семей которые старше 14 лет:\n" + RESET);
        allChilds(c);
        c.service.deleteAllChildrenOlderThen(14);
        log(YELLOW + "---------------------------------------------------------------------------------\n" + RESET);
        log(GREEN + "Удалить семью по индексу 3\n" + RESET);
        c.service.deleteFamilyByIndex(3);
        //c.service.displayAllFamilies();
    }
}
