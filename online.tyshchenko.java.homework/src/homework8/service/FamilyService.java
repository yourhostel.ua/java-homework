package homework8.service;

import homework8.dao.CollectionFamilyDao;
import homework8.model.baseClasses.Family;
import homework8.model.baseClasses.Human;
import homework8.model.baseClasses.Pet;
import homework8.dao.FamilyDao;
import homework8.model.derivativesFromHuman.Man;
import homework8.model.derivativesFromHuman.Woman;
import libs.Console;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.function.Predicate;

import static libs.Console.f;
import static libs.MethodMath.random;

public class FamilyService {
    public static final String RED = "\u001B[31m";
    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";
    public FamilyDao service;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.service = collectionFamilyDao;
    }

    public List<Family> getAllFamilies() {
        return service.getAllFamilies();
    }

    //TODO start 10 homework

    public void displayAllFamilies() {
        service.getAllFamilies()
                .forEach(Console::ln);
    }

    public void getFamiliesBiggerThan(int count) {
        service.getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() > count)
                .forEach(Console::ln);
    }

    public void getFamiliesLessThan(int count) {
        service.getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() < count)
                .forEach(Console::ln);
    }

    public void countFamiliesWithMemberNumber(int count) {
        f("количество семей из %d чел - %d\n", count,
                service.getAllFamilies()
                        .stream()
                        .filter(x -> x.countFamily() == count)
                        .count());
    }

    public Family bornChild(Family f, String nameBoy, String nameGirl) {
        Predicate<Integer> boolInt = (s) -> s > 0;
        boolean b = boolInt.test(random(0, 1));
        if (b) f.addChild(new Man(0, nameBoy, f.getFamilyName()));
        if (!b) f.addChild(new Woman(0, nameGirl, f.getFamilyName()));

        //інша реалізація блоку що вище
        //        int i = random(0,1);
        //        switch (i){
        //            case 1 -> f.addChild(new Man(0,nameBoy));
        //            case 0 -> f.addChild(new Woman(0,nameGirl));
        //        }

        service.saveFamily(f);
        f(BLUE + "У семьи %s %s - %s\n",
                f.getFamilyName(),
                b ? "родился мальчик" : "родилась девочка",
                b ? nameBoy : nameGirl
        );
        return service.getFamilyByFamily(f);
    }

    public void deleteAllChildrenOlderThen(int count) {
        try {
            service.getAllFamilies()
                    .stream()
                    .filter(x -> x.getChildren().size() > 0) //відразу фільтрую сім'ї з порожніми колекціями children
                    .forEach(x -> x.getChildren().forEach(y -> {
                        if (y.getYear() >= count) {
                            x.deleteChild(y);
                            deleteAllChildrenOlderThen(count);  //рекурсивно викликаю функцію щоб компенсувати
                        }                                       //зміщення елементів в колекції під час видалення
                    }));
        } catch (ConcurrentModificationException ignored) { //приглушив ексепшн
        }                                                   //не заважає видалити всіх за умовою

//TODO - теж саме звичайним циклом

//        List<Family> f = service.getAllFamilies();        //з звичайним циклом ексепшен не вискакує
//        for (int i = 0; i < f.size(); i++) {              //не знайшов вирішення цієї проблеми
//            List<Human> w = f.get(i).getChildren();
//            for (int j = 0; j < w.size(); j++) {
//                if (w.get(j).getYear() >= count) {
//                    f.get(i).deleteChild(w.get(j));
//                    deleteAllChildrenOlderThen(count);
//                }
//            }
//        }
    }

    //TODO end 10 homework

    public void createNewFamily(Human parentA, Human parentB, String familyName) {
        service.saveFamily(new Family(parentA, parentB, familyName));
    }

    public Family addFamily(Family f) {//*
        service.saveFamily(f);
        return service.getFamilyByFamily(f);
    }

    public void deleteFamilyByIndex(int index) {
        f(RED + "семья %s удалена\n" + RESET, service.getFamilyByIndex(index).getFamilyName());
        service.deleteFamily(index);
    }

    public Family adoptChild(Family f, Human h) {
        f.addChild(h);
        service.saveFamily(f);
        return service.getFamilyByFamily(f);
    }

    public int count() {//*
        return service.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return service.getFamilyByIndex(index);
    }

    public List<Pet> getPets(int i) {
        return service.getFamilyByIndex(i).getPets();
    }

    public void addPet(int index, Pet p) {
        service.getFamilyByIndex(index).setPet(p);
    }
}
