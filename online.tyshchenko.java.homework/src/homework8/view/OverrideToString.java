package homework8.view;

import java.util.Map;

public class OverrideToString {

    public static String overrideToStringPets(HelpObj.HelpObjPet a) {
        return String.format("""
                        %s{
                        \tnickname = '%s',\s
                        \tage = %d,\s
                        \ttrickLevel = %d,\s
                        \thabits = %s
                        \tspecies = {
                        \tcanFly = %s
                        \tnumberOfLegs = %s
                        \thasFur = %s
                        \t\t}
                        \t}
                        """,
                a.species(), a.nickname(), a.age(),
                a.trickLevel(), a.habits(),
                a.canFly(), a.numberOfLegs(), a.hasFur());
    }

    public static String overrideToStringHumans(HelpObj.HelpObjHuman a) {
        return String.format("""
                        Human{\s
                        \tname = '%s',\s
                        \tsurname = '%s',\s
                        \tyear = %d,\s
                        \tiq = %d,\s
                        \tschedule = %s
                        }
                        """,
                a.name(), a.surname(), a.year(), a.iq(),
                deploymentMap(a.schedule()));
    }

    public static String overrideToStringFamily(HelpObj.HelpObjFamily a) {
        return String.format("""
                        \tFamily = %s{\s
                        \tparentA = %s %s,\s
                        \tparentB = %s %s,\s
                        \tchildren = %s,\s
                        \tpet = %s}
                        """,
                a.familyName(),
                a.parentA_Name(), a.parentA_Surname(),
                a.parentB_Name(), a.parentB_Surname(),
                a.children(), a.pet());
    }

    public static <K, V> String deploymentMap(Map<K, V> a) {
        StringBuilder b = new StringBuilder();
        for (Map.Entry<K, V> elem : a.entrySet()) {
            b.append(elem.getKey()).append(": ").append(elem.getValue()).append(", ");
        }

        return "[" + b + "]";
    }
}
