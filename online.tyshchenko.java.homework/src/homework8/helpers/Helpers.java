package homework8.helpers;

import homework8.controller.FamilyController;
import homework8.model.baseClasses.Family;
import homework8.model.baseClasses.Human;

import java.util.List;

import static libs.Console.log;

public class Helpers {
    public static void allChilds(FamilyController c) {
        List<Family> f = c.service.getAllFamilies();
        for (Family family : f) {
            List<Human> w = family.getChildren();
            for (Human human : w) {
                if (human.getYear() >= 0) {
                    log(human.getName() + " ");
                    log(human.getSurname() + " ");
                    log(human.getYear() + "\n");
                }
            }
        }
    }
}
