package homework8.model.derivativesFromHuman;

import homework8.model.baseClasses.Human;

import java.util.Map;

import static libs.Console.log;

public final class Woman extends Human {
    public Woman(int year, String... a) {
        super(year, a);
    }

    public Woman(int year, int iq, Map<String, String> schedule, String... a) {
        super(year, iq, schedule, a);
    }

    public Woman() {
    }

    public String makeup() {
        return "краситься";
    }

    @Override
    public void greetPet() {
        super.greetPet();
        log("Идем гулять?");
    }
}
