package homework8.model.derivativesFromPet;

import homework8.model.baseClasses.Pet;
import homework8.model.enumerations.Species;
import homework8.view.HelpObj;
import homework8.view.OverrideToString;

import java.util.Set;

import static libs.Console.log;

public class RoboCat extends Pet {

    public final Species species;

    public RoboCat(String nickname) {
        super(nickname);
        this.species = Species.UNKNOWN;
    }

    public RoboCat(Species species, String nickname) {
        super(nickname);
        this.species = species;
    }

    public RoboCat(int age, int trickLevel, Set<String> habits, Species species, String nickname) {
        super(age, trickLevel, habits, nickname);
        this.species = species;
    }

    public Species getSpecies() {
        return species;
    }

    @Override
    public void eat() {
        log(String.format("Я кот-робот - %s, я заряжаю свои батареи.\n", this.getNickname()));
    }

    @Override
    public void respond() {
        log(String.format("Я кот-робот - %s.\n", this.getNickname()));
    }

    @Override
    public String toString() {
        return OverrideToString.overrideToStringPets(
                new HelpObj.HelpObjPet(
                        this.species, this.getNickname(), this.getAge(),
                        this.getTrickLevel(), this.getHabits(), this.species.canFly,
                        this.species.numberOfLegs, this.species.hasFur));
    }
}
