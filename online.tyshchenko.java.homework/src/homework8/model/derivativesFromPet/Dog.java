package homework8.model.derivativesFromPet;

import homework8.model.baseClasses.Pet;
import homework8.model.enumerations.Species;
import homework8.model.derivativesFromPet.peculiarPropertiesPets.TrickyPets;
import homework8.view.HelpObj;
import homework8.view.OverrideToString;

import java.util.Set;

import static libs.Console.log;

public class Dog extends Pet implements TrickyPets {

    public final Species species;

    public Dog(String nickname) {
        super(nickname);
        this.species = Species.UNKNOWN;
    }

    public Dog(Species species, String nickname) {
        super(nickname);
        this.species = species;
    }

    public Dog(int age, int trickLevel, Set<String> habits, Species species, String nickname) {
        super(age, trickLevel, habits, nickname);
        this.species = species;
    }

    public Species getSpecies() {
        return species;
    }

    public void foul() {
        log("Нужно хорошо замести следы...\n");
    }

    @Override
    public void eat() {
        log("Я кушаю!\n");
    }

    @Override
    public void respond() {
        log(String.format("Привет, хозяин. Я - %s. Я соскучился!\n", this.getNickname()));
    }

    @Override
    public String toString() {
        return OverrideToString.overrideToStringPets(
                new HelpObj.HelpObjPet(
                        this.species, this.getNickname(), this.getAge(),
                        this.getTrickLevel(), this.getHabits(), this.species.canFly,
                        this.species.numberOfLegs, this.species.hasFur));
    }
}
