package homework8.dao;

import homework8.model.baseClasses.Family;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);//бо null

    Family getFamilyByFamily(Family f);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);
}
