package homework8.dao;

import homework8.model.baseClasses.Family;

import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private final List<Family> family;

    public CollectionFamilyDao(List<Family> family) {
        this.family = family;
    }

    @Override
    public List<Family> getAllFamilies() {
        return family;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return family.get(index);
    }

    public Family getFamilyByFamily(Family f) {
        return family.get(family.indexOf(f));
    }

    @Override
    public boolean deleteFamily(int index) {
        Family f = family.remove(index);
        return f != null;
    }

    @Override
    public boolean deleteFamily(Family f) {
        return family.remove(f);
    }

    @Override
    public void saveFamily(Family f) {
        if (family.contains(f)) {
            family.set(family.indexOf(f), f);
        } else {
            //family.set(family.size() + 1, f);
            family.add(f);
        }
    }
}
