package homework3;

import static libs.Console.nextLine;
import static libs.Console.log;

import java.text.DateFormatSymbols;
import java.util.Locale;

public class WeeklyTaskPlanner {
    static String[] tasks = {"Do home work", "Go to courses; Watch a film", "Go to explore the mountains", "Suppress the rebellion", "Planned trip to London", "Discuss the need for a new social contract with shared responsibility", "To do our part to improve the world around us"};

    public static void main(String[] args) {
        Tasks todo = new Tasks(tasks);
        todo.of();
        do {
            log("Please, input the day of the week\n");
            if (todo.getTask(nextLine())) return;
        } while (true);
    }

    public static class Tasks {
        private final String[][] scedule;
        private final String[] weekdays;
        private final String[] task;

        Tasks(String[] task) {
            this.weekdays = new DateFormatSymbols(Locale.of("en", "Uk")).getWeekdays();
            this.scedule = new String[7][2];
            this.task = task;
        }

        private void of() {
            for (int i = 0; i < scedule.length; i++) {
                this.scedule[i][0] = this.weekdays[i + 1];
                this.scedule[i][1] = this.task[i];
            }
        }

        private boolean getTask(String cmd) {
            String PURPLE = "\u001B[35m";
            final String RESET = "\u001B[0m";
            String dayOfWeek = null, task = null;
            boolean bool = true, isExit = false;

            switch (cmd.toLowerCase()) {
                case "sunday" -> {
                    dayOfWeek = this.scedule[0][0];
                    task = this.scedule[0][1];
                }
                case "monday" -> {
                    dayOfWeek = this.scedule[1][0];
                    task = this.scedule[1][1];
                }
                case "tuesday" -> {
                    dayOfWeek = this.scedule[2][0];
                    task = this.scedule[2][1];
                }
                case "wednesday" -> {
                    dayOfWeek = this.scedule[3][0];
                    task = this.scedule[3][1];
                }
                case "thursday" -> {
                    dayOfWeek = this.scedule[4][0];
                    task = this.scedule[4][1];
                }
                case "friday" -> {
                    dayOfWeek = this.scedule[5][0];
                    task = this.scedule[5][1];
                }
                case "saturday" -> {
                    dayOfWeek = this.scedule[6][0];
                    task = this.scedule[6][1];
                }
                case "exit" -> isExit = true;
                default -> bool = false;
            }
            if (bool && !isExit) log(String.format(PURPLE + "Your tasks for %s: %s.\n", dayOfWeek, task + RESET));
            if (!bool) log("Sorry, I don't understand you, please try again.\n");
            return isExit;
        }
    }
}
