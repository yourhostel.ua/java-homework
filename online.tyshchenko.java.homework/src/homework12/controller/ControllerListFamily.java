package homework12.controller;

import homework12.model.baseClasses.Family;
import homework12.model.baseClasses.Human;
import homework12.model.baseClasses.Pet;
import homework12.service.ServiceListFamily;

import java.util.List;

public class ControllerListFamily {
    public ServiceListFamily service;

    public ControllerListFamily(ServiceListFamily service) {
        this.service = service;
    }

    public List<Family> getAllFamilies() {
        return service.getAllFamilies();
    }

    public void displayAllFamilies() {
        service.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int count) {
        service.getFamiliesBiggerThan(count);
    }

    public void getFamiliesLessThan(int count) {
        service.getFamiliesLessThan(count);
    }

    public void countFamiliesWithMemberNumber(int count) {
        service.countFamiliesWithMemberNumber(count);
    }

    public Family bornChild(Family f, String nameBoy, String nameGirl) {
        return service.bornChild(f, nameBoy, nameGirl);
    }

    public void deleteAllChildrenOlderThen(int count) {
        service.deleteAllChildrenOlderThen(count);
    }

    public void createNewFamily(Human parentA, Human parentB, String familyName) {
        service.createNewFamily(parentA, parentB, familyName);
    }

    public Family addFamily(Family f) {
        return service.addFamily(f);
    }

    public boolean deleteFamilyByIndex(int index) {
        return service.deleteFamilyByIndex(index);
    }

    public boolean deleteFamilyByFamily(Family f) {
        return service.deleteFamilyByFamily(f);
    }

    public Family adoptChild(Family f, Human h) {
        return service.adoptChild(f, h);
    }

    public int count() {//*
        return service.count();
    }

    public Family getFamilyById(int index) {
        return service.getFamilyById(index);
    }

    public List<Pet> getPets(int i) {
        return service.getPets(i);
    }

    public void addPet(int index, Pet p) {
        service.addPet(index, p);
    }
}
