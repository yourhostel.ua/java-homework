package homework12.controller;

import homework12.model.baseClasses.Family;
import homework12.service.ServiceFileFamily;

import java.util.List;

public class ControllerFileFamily {
    public ServiceFileFamily service;

    public ControllerFileFamily(ServiceFileFamily service) {
        this.service = service;
    }

    public boolean setList(List<Family> as) {
        return service.setList(as);
    }

    public List<Family> getAllFamilies() {
        return service.getAll();
    }

    public int count() {
        return service.count();
    }

    public Family getFamilyById(int index) {
        return service.getFamilyById(index);
    }
}
