package homework12.doMain.menu;

import homework12.controller.ControllerFileFamily;
import homework12.controller.ControllerListFamily;
import homework12.model.baseClasses.Family;

import java.util.List;

import static homework12.doMain.gen.Gen.gen;
import static homework12.view.Header.head;
import static libs.Console.*;

public class StoreMenu {
    static void persist(ControllerListFamily l, ControllerFileFamily f) {
        while (true) {
            head(12);
            switch (valid(nextLine())) {
                case 1 -> sub1(l, f);//загрузить из файла
                case 2 -> gen(l);//ген семей в коллекцию
                case 3 -> sub2(f);//показать семьи файла
                case 4 -> sub3(l, f);//сохранить семьи в файл
                case 5 -> sub4(f);//показать семью по индексу из файла
                case 6 -> {
                    return;//Вернуться в главное меню
                }
            }
        }
    }

    static void sub1(ControllerListFamily l, ControllerFileFamily f) {
        List<Family> families = f.getAllFamilies();
        if (families != null) {
            for (Family family : families) l.addFamily(family);
        }
        ln("Семьи загружены из файла в коллекцию");
    }

    static void sub2(ControllerFileFamily f) {
        if (f.getAllFamilies() != null) {
            f.getAllFamilies().forEach(family -> ln(family.prettyFormat()));
        }
    }

    static void sub3(ControllerListFamily c, ControllerFileFamily f) {
        if (c.getAllFamilies().size() == 0) {
            ln("Коллекция пустая, создайте или сгенерируйте семьи");
        } else if (f.setList(c.getAllFamilies())) ln("Дынные о семьях перенесены в файл");
    }

    static void sub4(ControllerFileFamily f) {
        int count = f.count();
        if (count != 0) {
            f("Введите индекс семьи от 0 до %d\n", count - 1);
            int cmd = 0;
            try {
                cmd = Integer.parseInt(nextLine());
            } catch (NumberFormatException e) {
                f("%s - не число\n", e.getMessage());
            } catch (RuntimeException e) {
                ln(e);
            }
            if (cmd >= count || cmd < 0) f("Значение '%d' вне диапазона от 0 до %d\n", cmd, count - 1);
            else ln(f.getFamilyById(cmd).prettyFormat());
        }
    }

    static int valid(String s) {
        int cmd = 0;
        try {
            cmd = Integer.parseInt(s);
            if (cmd > 6 || cmd < 1) {
                ln("Такой команды нет");
            }
        } catch (NumberFormatException e) {
            f("%s - не число\n", e.getMessage());
        }
        return cmd;
    }
}
