package homework12.doMain.menu;

import homework12.controller.ControllerFileFamily;
import homework12.controller.ControllerListFamily;
import homework12.dao.DaoFile;
import homework12.dao.DaoList;
import homework12.service.ServiceFileFamily;
import homework12.service.ServiceListFamily;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import static homework12.doMain.menu.StoreMenu.persist;
import static homework12.doMain.menu.EditMenu.*;
import static libs.Console.f;
import static libs.Console.ln;

public class MainMenu {
    static ControllerListFamily controller = new ControllerListFamily(new ServiceListFamily(new DaoList<>(new ArrayList<>())));

    static ControllerFileFamily controllerFile = new ControllerFileFamily(new ServiceFileFamily(new DaoFile<>(getFile(new File("family.bin")))));

    public static void menu(String i) {
        switch (i) {
            case "1" -> persist(controller, controllerFile);
            case "2" -> sub2(controller);
            case "3" -> sub3(controller, ">");
            case "4" -> sub3(controller, "<");
            case "5" -> sub3(controller, "=");
            case "6" -> sub4(controller);
            case "7" -> sub5(controller);
            case "8" -> sub6(controller);
            case "9" -> sub7(controller);
        }
    }

    public static boolean valid(String s) {
        try {
            int cmd = Integer.parseInt(s);
            if (cmd > 9 || cmd < 1) {
                ln("Такой команды нет");
                return false;
            }
            if (controller.getAllFamilies().size() == 0 && cmd > 1 && cmd != 6) {
                ln("Вы должны сначала сгенерировать семьи командой - 1");
                ln("Или добавить хотя-бы одну семью командой  - 6");
                return false;
            }
        } catch (NumberFormatException e) {
            f("%s - не число\n", e.getMessage());
            return false;
        }
        return true;
    }

    public static File getFile(File file) {
        if (Files.exists(Paths.get(file.toURI()))) {
            return file;
        } else {
            try {
                file = Files.createFile(Path.of("family.bin")).toFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return file;
    }
}
