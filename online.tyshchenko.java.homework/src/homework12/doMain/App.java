package homework12.doMain;

import java.util.Objects;

import static homework12.doMain.menu.MainMenu.menu;
import static homework12.doMain.menu.MainMenu.valid;
import static homework12.view.Header.head;
import static libs.Console.ln;
import static libs.Console.nextLine;

public class App {
    static public void run() {
        while (true) {
            head(0);
            String cmd = nextLine();
            if (Objects.equals(cmd, "exit")) {
                ln("program completed");
                break;
            }
            if (valid(cmd)) {
                menu(cmd);
            }
        }
    }
}
