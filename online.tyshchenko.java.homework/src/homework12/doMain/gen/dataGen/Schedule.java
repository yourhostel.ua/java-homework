package homework12.doMain.gen.dataGen;

public class Schedule {
    public static final String[] shm = new String[]
            {
                    "meeting with partners",
                    "construction works",
                    "purchase of raw materials",
                    "going to the supermarket",
                    "flight to London",
                    "to the vet",
                    "meeting with the boss",
                    "corporate party",
                    "it courses",
                    "fishing",
                    "meeting",
                    "rest in the park",
                    "going to the theater",
                    "pick up the parcel",
                    "going to the bank",
                    "real estate market research",
                    "cleaning",
                    "swimming section",
                    "new project",
                    "meeting with classmates",
            };
}
