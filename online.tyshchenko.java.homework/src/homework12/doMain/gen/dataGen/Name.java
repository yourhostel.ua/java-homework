package homework12.doMain.gen.dataGen;

public class Name {
    public static final String[] femaleName = new String[]
            {
                    "Anna",
                    "Maria",
                    "Sophia",
                    "Katherine",
                    "Victoria",
                    "Alexandra",
                    "Eva",
                    "Diana",
                    "Melanie",
                    "Florence",
                    "Agatha",
                    "Rebecca",
                    "Barbara",
                    "Amanda",
                    "Irene",
                    "Margaret",
                    "Deborah",
                    "Emma",
                    "Stacy",
                    "Ruth",
            };

    public static final String[] maleName = new String[]
            {
                    "Oliver",
                    "Jack",
                    "Harry",
                    "Jacob",
                    "Charley",
                    "Thomas",
                    "George",
                    "Oscar",
                    "James",
                    "William",
                    "Liam",
                    "Mason",
                    "Ethan",
                    "Michael",
                    "Alexander",
                    "James",
                    "Daniel",
                    "Robert",
                    "Benjamin",
                    "Lewis",
            };

    public static final String[] surname = new String[]
            {
                    "Williams",
                    "Roberts",
                    "Peters",
                    "Mills",
                    "Gibson",
                    "Martin",
                    "Jordan",
                    "Jackson",
                    "Lewis",
                    "Grant",
                    "Morgan",
                    "Florence",
                    "Davis",
                    "Campbell",
                    "Bronte",
                    "Collins",
                    "Bradley",
                    "Bell",
                    "Adams",
                    "Barlow",
            };
}
