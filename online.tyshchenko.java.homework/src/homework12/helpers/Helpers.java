package homework12.helpers;

import homework12.controller.ControllerListFamily;
import homework12.model.baseClasses.Family;
import homework12.model.baseClasses.Human;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.List;

import static homework12.view.OverrideToString.deploymentDate;
import static libs.Console.ln;
import static libs.Console.log;

public class Helpers {
    public static void allChild(ControllerListFamily c) {
        long l = Timestamp.valueOf(LocalDate.now()
                .minusYears(0).atStartOfDay()).getTime();
        List<Family> f = c.service.getAllFamilies();
        log("Все дети:\n");
        for (Family family : f) {
            List<Human> w = family.getChildren();
            for (Human human : w) {
                if (human.getBirthDate() <= l) {
                    log(human.getName() + " ");
                    log(human.getSurname() + " ");
                    log(" год рождения: " + deploymentDate(human.getBirthDate()) + "");
                    log(" Возраст: " + yearOf(human.getBirthDate()) + "\n");
                }
            }
        }
        log("Удалены по условию:\n");
    }

    public static long millisOf(String birthDate) {
        int y = 0, m = 0, d = 0;
        try {
            String[] b = birthDate.split("/");
            y = Integer.parseInt(b[0]);
            m = Integer.parseInt(b[1]);
            d = Integer.parseInt(b[2]);
        } catch (NumberFormatException e) {
            ln("Не валидное значение");
            ln(e.getMessage());
        }
        return Timestamp.valueOf(LocalDate.of(d, m, y).atStartOfDay()).getTime();
    }

    public static int yearOf(long birthDate) {
        LocalDate startDate = LocalDate.from(Instant
                .ofEpochMilli(birthDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime());
        LocalDate endDate = LocalDate.now();
        Period period = Period.between(startDate, endDate);
        return period.getYears();
    }
}
