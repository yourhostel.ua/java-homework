package homework12.dao;

import java.util.List;

public interface Dao<T> {

    List<T> getAll();

    T getByIndex(int o);

    T getByToObj(T o);

    boolean delete(int o);

    boolean delete(T o);

    void save(T o);
}
