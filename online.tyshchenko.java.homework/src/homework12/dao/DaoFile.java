package homework12.dao;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static libs.Console.f;
import static libs.Console.ln;
import static libs.EX.NI;

public class DaoFile<T extends Serializable> implements Dao<T>, extraFile<T> {

    private final File file;

    public DaoFile(File file) {
        this.file = file;
    }

    @Override
    public List<T> getAll() {
        ArrayList<T> as = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            as = (ArrayList<T>) ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            ln("Файл пуст или отсутствует!");
        }
        return as;
    }

    @Override
    public boolean setList(List<T> as) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(as);
            oos.close();
        } catch (IOException e) {
            f("При записи коллекции в файл произошла ошибка %s\n", e);
            return false;
        }
        return true;
    }

    @Override
    public T getByIndex(int i) {
        ArrayList<T> as;
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            as = (ArrayList<T>) ois.readObject();
            ois.close();
            return as.get(i);
        } catch (IOException | ClassNotFoundException | IllegalStateException e) {
            ln(e.getMessage());
        }
        return null;
    }

    @Override
    public T getByToObj(T o) {
        throw NI;
    }

    @Override
    public boolean delete(int i) {
        ArrayList<T> as;
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            as = (ArrayList<T>) ois.readObject();
            ois.close();
            as.remove(i);
        } catch (IOException | ClassNotFoundException e) {
            f("При удалении произошла ошибка %s\n", e.getMessage());
            return false;
        }

        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(as);
            oos.close();
        } catch (IOException e) {
            f("При возврате коллекции в файл после " +
                    "удаления произошла ошибка %s\n", e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(T o) {
        throw NI;
    }

    @Override
    public void save(T o) {
        ArrayList<T> as;

        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            as = (ArrayList<T>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            f("десериализовать коллекцию из файла " +
                    "не удалось была создана новая колекция %s\n", e.getMessage());
            as = new ArrayList<>();
        }

        as.add(o);

        try (FileOutputStream fos = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(as);
            ln("Объект успешно добавлен");
        } catch (IOException e) {
            f("Сохранить объект не удалось %s\n", e.getMessage());
        }
    }
}
