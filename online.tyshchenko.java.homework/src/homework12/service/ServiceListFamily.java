package homework12.service;

import homework12.customException.FamilyOverflowException;
import homework12.dao.DaoList;
import homework12.model.baseClasses.Family;
import homework12.model.baseClasses.Human;
import homework12.model.baseClasses.Pet;
import homework12.model.derivativesFromHuman.Man;
import homework12.model.derivativesFromHuman.Woman;
import libs.Console;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.function.Predicate;

import static libs.Console.f;
import static libs.MethodMath.random;

public class ServiceListFamily {
    public static final String RED = "\u001B[31m";
    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";
    private final DaoList<Family> dao;

    public ServiceListFamily(DaoList<Family> dao) {
        this.dao = dao;
    }

    public List<Family> getAllFamilies() {
        return dao.getAll();
    }

    public void displayAllFamilies() {
        dao.getAll()
                .forEach(Console::ln);
    }

    public void getFamiliesBiggerThan(int count) {
        dao.getAll()
                .stream()
                .filter(x -> x.countFamily() > count)
                .forEach(Console::ln);
    }

    public void getFamiliesLessThan(int count) {
        dao.getAll()
                .stream()
                .filter(x -> x.countFamily() < count)
                .forEach(Console::ln);
    }

    public void countFamiliesWithMemberNumber(int count) {
        f("количество семей из %d чел - %d\n", count,
                dao.getAll()
                        .stream()
                        .filter(x -> x.countFamily() == count)
                        .count());
    }

    public Family bornChild(Family f, String nameBoy, String nameGirl) {
        Predicate<Integer> boolInt = (s) -> s > 0;
        boolean b = boolInt.test(random(0, 1));
        if (b)
            f.addChild(new Man(Timestamp
                    .valueOf(LocalDate.now().atStartOfDay()).getTime(),
                    nameBoy,
                    f.getFamilyName())
            );
        if (!b)
            f.addChild(new Woman(Timestamp
                    .valueOf(LocalDate.now().atStartOfDay()).getTime(),
                    nameGirl,
                    f.getFamilyName())
            );

        dao.save(f);

        f(BLUE + "У семьи %s %s - %s\n",
                f.getFamilyName(),
                b ? "родился мальчик" : "родилась девочка",
                b ? nameBoy : nameGirl
        );
        return dao.getByToObj(f);
    }

    public void deleteAllChildrenOlderThen(int count) {
        long l = Timestamp.valueOf(LocalDate.now()
                .minusYears(count).atStartOfDay()).getTime();
        try {
            dao.getAll()
                    .stream()
                    .filter(x -> x.getChildren().size() > 0)
                    .forEach(x -> x.getChildren().forEach(y -> {
                        if (y.getBirthDate() <= l) {
                            x.deleteChild(y);
                            deleteAllChildrenOlderThen(count);
                        }
                    }));
        } catch (ConcurrentModificationException ignored) {
        }
    }

    public void createNewFamily(Human parentA, Human parentB, String familyName) {
        dao.save(new Family(parentA, parentB, familyName));
    }

    public Family addFamily(Family f) {
        dao.save(f);
        return dao.getByToObj(f);
    }

    public boolean deleteFamilyByIndex(int index) {
        f(RED + "семья %s удалена\n" + RESET,
                dao.getByIndex(index).getFamilyName());
        return dao.delete(index);
    }

    public boolean deleteFamilyByFamily(Family f) {
        f(RED + "семья %s удалена\n" + RESET, f.getFamilyName());
        return dao.delete(f);
    }

    public Family adoptChild(Family f, Human h) {
        if (f.countFamily() == 5) {
            throw new FamilyOverflowException("Ограничение до 3 детей");
        }
        f.addChild(h);
        dao.save(f);
        return dao.getByToObj(f);
    }

    public int count() {
        return dao.getAll().size();
    }

    public Family getFamilyById(int index) {
        return dao.getByIndex(index);
    }

    public List<Pet> getPets(int i) {
        return dao.getByIndex(i).getPets();
    }

    public void addPet(int index, Pet p) {
        dao.getByIndex(index).setPet(p);
    }
}
