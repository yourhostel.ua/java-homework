package homework12.service;

import homework12.dao.DaoFile;
import homework12.model.baseClasses.Family;

import java.util.List;

public class ServiceFileFamily {
    private final DaoFile<Family> dao;

    public ServiceFileFamily(DaoFile<Family> dao) {
        this.dao = dao;
    }

    public boolean setList(List<Family> as) {
        return dao.setList(as);
    }

    public List<Family> getAll() {
        return dao.getAll();
    }

    public int count() {
        if (dao.getAll() != null) {
            return dao.getAll().size();
        }
        return 0;
    }

    public Family getFamilyById(int i) {
        return dao.getByIndex(i);
    }
}
