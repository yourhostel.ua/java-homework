package homework12.view;

import static libs.Console.f;
import static libs.Console.ln;

public class Header {
    public static final String GREEN = "\u001B[32m";
    public static final String CYAN = "\u001B[36m";
    public static final String RESET = "\u001B[0m";

    public static void head(int j) {
        String m = null;
        int k = 9;
        switch (j) {
            case 0 -> m = "Happy family";
            case 9 -> {
                m = "Edit  family";
                k = 12;
            }
            case 12 -> {
                m = "file  family";
                k = 18;
            }
        }
        headView(j, k, m);
    }

    public static void headView(int j, int k, String m) {
        f(GREEN + "==========================================" + CYAN + " %S " + RESET + GREEN + "=============================================\n" + RESET, m);
        for (; j < k; j += 2) {
            if (j != k - 1) f("  %-48s   %s\n", points[j], points[j + 1]);
            else f("  %s\n", points[j]);
        }
        ln(GREEN + "=====================================================================================================" + RESET);
    }

    public static final String[] points = new String[]
            {
                    //----- Happy family ---------
                    "- 1. Работа с файлом и генерация семей",
                    "- 2. Отобразить весь список семей",
                    "- 3. Отобразить список семей кол-во членов > i",
                    "- 4. Отобразить список семей кол-во членов < i",
                    "- 5. Кол-во семей где кол-во членов = i",
                    "- 6. Создать новую семью",
                    "- 7. Удалить семью по индексу i",
                    "- 8. Редактировать семью по индексу",
                    "- 9. Удалить всех детей старше i",
                    //----- Edit  family ---------
                    "- 1. Родить ребенка в семье i ",
                    "- 2. Усыновить ребенка в семью i",
                    "- 3. Вернуться в главное меню",
                    //----- file  family ---------
                    "- 1. Загрузить из файла",
                    "- 2. Сгенерировать семьи в коллекцию",
                    "- 3. Показать семьи файла",
                    "- 4. Сохранить семьи в файл",
                    "- 5. Показать семью по индексу из файла",
                    "- 6. Вернуться в главное меню",
            };
}
