package homework12.model.baseClasses;

import homework12.model.enumerations.Species;
import homework12.view.HelpObj;
import homework12.view.OverrideToString;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import static homework12.view.OverrideToString.petPrettyFormat;
import static libs.Console.err;
import static libs.Console.log;

public abstract class Pet implements Serializable {

    public final String nickname;
    private final Set<String> habits = new TreeSet<>();
    private int age;
    private int trickLevel;
    private final Species species;

    public Pet(int age, int trickLevel, Set<String> habits, String nickname, Species species) {
        this.age = age;
        this.species = species;
        this.habits.addAll(habits);
        this.nickname = nickname;
        setTrickLevel(trickLevel); //Якщо не "final" можна встановити сеттером без присвоєння в конструкторі, установка через сеттер буде доступна.
    }

    public Pet(String nickname) {
        this.species = Species.UNKNOWN;
        this.age = 0;
        this.nickname = nickname;
        this.trickLevel = 0;
    }

    public Pet(String nickname, Species species) {
        this.species = species;
        this.age = 0;
        this.nickname = nickname;
        this.trickLevel = 0;
    }

    public Species getSpecies() {
        return this.species;
    }

    public int getAge() {
        return this.age;
    }

    public Set<String> getHabits() {
        return this.habits;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHabits(String habits) {
        this.habits.add(habits);
    }

    public void setTrickLevel(int trickLevel) {
        try {
            if (trickLevel >= 0 && trickLevel <= 100) this.trickLevel = trickLevel;
            else
                throw new Exception(String
                        .format(" Value not set! %d is not valid. \"trickLevel\" should be from 0 to 100\n",
                                trickLevel));
        } catch (Exception e) {
            err(e.getMessage());
        }
    }

    public String prettyFormat() {
        return petPrettyFormat(new HelpObj.ppf(this.species,
                this.nickname, this.age, this.trickLevel, this.habits));
    }

    @Override
    public String toString() {
        return OverrideToString.overrideToStringPets(
                new HelpObj.HelpObjPet(
                        this.getSpecies(), this.getNickname(), this.getAge(),
                        this.getTrickLevel(), this.getHabits(), this.getSpecies().canFly,
                        this.getSpecies().numberOfLegs, this.getSpecies().hasFur));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet pet = (Pet) o;

        if (trickLevel != pet.trickLevel) return false;
        return nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        int result = nickname.hashCode();
        result = 31 * result + trickLevel;
        return result;
    }

    public void eat() {
        log("");
    }

    public void respond() {
        log("");
    }
}
