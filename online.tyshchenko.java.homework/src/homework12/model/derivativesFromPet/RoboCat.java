package homework12.model.derivativesFromPet;

import homework12.model.baseClasses.Pet;
import homework12.model.enumerations.Species;

import java.util.Set;

import static libs.Console.log;

public class RoboCat extends Pet {

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(Species species, String nickname) {
        super(nickname, species);
    }

    public RoboCat(int age, int trickLevel, Set<String> habits, Species species, String nickname) {
        super(age, trickLevel, habits, nickname, species);
    }

    @Override
    public void eat() {
        log(String.format("Я кот-робот - %s, я заряжаю свои батареи.\n",
                this.getNickname()));
    }

    @Override
    public void respond() {
        log(String.format("Я кот-робот - %s.\n",
                this.getNickname()));
    }
}
