package homework12.model.derivativesFromHuman;

import homework12.model.baseClasses.Human;

import java.util.Map;

import static libs.Console.log;

public final class Woman extends Human {
    public Woman(String year, String... a) {
        super(year, a);
    }

    public Woman(long year, String... a) {
        super(year, a);
    }

    public Woman(String year, int iq, Map<String, String> schedule, String... a) {
        super(year, iq, schedule, a);
    }

    public Woman(String year, int iq, String... a) {
        super(year, iq, a);
    }

    public Woman() {
    }

    public String makeup() {
        return "краситься";
    }

    @Override
    public void greetPet() {
        super.greetPet();
        log("Идем гулять?");
    }
}
