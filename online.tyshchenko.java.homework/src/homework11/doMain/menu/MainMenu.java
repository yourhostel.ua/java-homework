package homework11.doMain.menu;

import homework11.controller.FamilyController;
import homework11.dao.DaoList;
import homework11.service.FamilyService;

import java.util.ArrayList;

import static homework11.doMain.gen.Gen.gen;
import static homework11.doMain.menu.SubMenu.*;
import static homework11.view.Header.mainHead;
import static libs.Console.f;
import static libs.Console.ln;

public class MainMenu {
    static FamilyController controller = new FamilyController(new FamilyService(new DaoList<>(new ArrayList<>())));

    public static void menu(String i) {
        switch (i) {
            case "1" -> gen(controller);
            case "2" -> sub2(controller);
            case "3" -> sub3(controller, ">");
            case "4" -> sub3(controller, "<");
            case "5" -> sub3(controller, "=");
            case "6" -> sub4(controller);
            case "7" -> sub5(controller);
            case "8" -> sub6(controller);
            case "9" -> sub7(controller);
        }
    }

    public static boolean valid(String s) {
        try {
            int cmd = Integer.parseInt(s);
            if (cmd > 9 || cmd < 1) {
                ln("Такой команды нет");
                mainHead(0);
                return false;
            }
            if (controller.getAllFamilies().size() == 0 && cmd > 1 && cmd != 6) {
                ln("Вы должны сначала сгенерировать семьи командой - 1");
                ln("Или добавить хотя-бы одну семью командой  - 6");
                mainHead(0);
                return false;
            }
        } catch (NumberFormatException e) {
            f("%s - не число\n", e.getMessage());
            mainHead(0);
            return false;
        }
        mainHead(0);
        return true;
    }
}
