package homework11.doMain.menu;

import homework11.controller.FamilyController;
import homework11.model.baseClasses.Family;
import homework11.model.baseClasses.Human;
import homework11.model.derivativesFromHuman.Man;
import homework11.model.derivativesFromHuman.Woman;

import java.util.Objects;

import static homework11.view.Header.mainHead;
import static libs.Console.*;

public class SubMenu {
    static void sub2(FamilyController c) {
        c.getAllFamilies().forEach(f -> ln(f.prettyFormat()));
        mainHead(0);
    }

    static void sub3(FamilyController c, String s) {
        f("Введите i. кол-во членов семьи %s i\n", s);
        try {
            int i = Integer.parseInt(nextLine());
            c.getAllFamilies()
                    .stream()
                    .filter(f -> subThan(f, s, i))
                    .forEach(f -> ln(f.prettyFormat()));
            mainHead(0);
        } catch (NumberFormatException e) {
            f("%s - не число\n", e.getMessage());
        }
    }

    static boolean subThan(Family f, String s, int i) {
        boolean b = true;
        switch (s) {
            case ">" -> b = f.countFamily() > i;
            case "<" -> b = f.countFamily() < i;
            case "=" -> b = f.countFamily() == i;
        }
        return b;
    }

    public static void sub4(FamilyController c) {
        Human parentA = logSub4("матери");
        Human parentB = logSub4("отца");
        c.addFamily(new Family(parentA, parentB, parentB.getSurname()));
        ln("Семья успешно создана");
        mainHead(0);
    }

    static Human logSub4(String s) {
        String[] h = new String[2];
        int[] j = new int[4];
        String[] q = new String[]{"имя", "фамилию", "год рождения", "месяц рождения", "день рождения", "iq"};

        for (int i = 0; i < 6; i++) {
            f("Введите %s %s\n", q[i], s);
            if (i <= 1) {
                while (true) {
                    String p = nextLine();
                    try {
                        Integer.parseInt(p);
                        f("%s - значение должно быть строкой\n", p);
                        f("Введите %s %s\n", q[i], s);
                    } catch (NumberFormatException e) {
                        h[i] = p;
                        break;
                    }
                }
            }

            if (i > 1) {
                while (true) {
                    try {
                        int v = Integer.parseInt(nextLine());
                        j[i - 2] = v;
                        break;
                    } catch (NumberFormatException e) {
                        f("%s - не число\n", e.getMessage());
                        f("Введите %s %s\n", q[i], s);
                    }
                }
            }
        }
        Human res = null;
        String date = String.format("%d/%d/%d", j[2], j[1], j[0]);
        if (Objects.equals(s, "матери")) res = new Woman(date, j[3], h[0], h[1]);
        if (Objects.equals(s, "отца")) res = new Man(date, j[3], h[0], h[1]);
        if (Objects.equals(s, "девочки")) res = new Woman(date, j[3], h[0], h[1]);
        if (Objects.equals(s, "мальчика")) res = new Man(date, j[3], h[0], h[1]);
        return res;
    }

    public static void sub5(FamilyController c) {
        ln("Введите индекс семьи для удаления");
        try {
            try {
                int i = Integer.parseInt(nextLine());
                if (i > c.count())
                    throw new Exception(String.format("Введённый индекс '%s' больше кол-ва семей в коллекции (%d)", i, c.count()));
                c.deleteFamilyByIndex(i);
            } catch (NumberFormatException e) {
                f("%s\n", e.getMessage());
            }
        } catch (Exception e) {
            ln(e.getMessage());
        }
        mainHead(0);
    }

    public static void sub6(FamilyController c) {
        while (true) {
            mainHead(9);
            String i = nextLine();
            switch (i) {
                case "1" -> sub6_1(c);
                case "2" -> sub6_2(c);
                case "3" -> {
                    mainHead(0);
                    return;
                }
            }
        }
    }

    public static void sub6_1(FamilyController c) {
        int i;
        String s;
        while (true) {
            ln("Введите порядковый номер семьи");
            try {
                try {
                    i = Integer.parseInt(nextLine());
                    if (i >= c.count())
                        throw new Exception(String.format(String.format("Введённый индекс '%s' больше кол-ва семей в коллекции (%d)", i, c.count())));
                    while (true) {
                        ln("Введите имя мальчика и девочки через пробел");
                        s = nextLine();
                        try {
                            if (s.split(" ").length != 2) throw new Exception(s);
                            c.bornChild(c.getFamilyById(i), s.split(" ")[0], s.split(" ")[1]);
                            return;
                        } catch (Exception ex) {
                            f("%s - должно быть два слова через пробел\n", ex.getMessage());
                        }
                    }
                } catch (NumberFormatException e) {
                    f("%s\n", e.getMessage());
                }
            } catch (Exception e) {
                ln(e.getMessage());
            }
        }
    }

    public static void sub6_2(FamilyController c) {
        int i;
        String s;
        while (true) {
            ln("Введите порядковый номер семьи");
            try {
                i = Integer.parseInt(nextLine());
                if (i >= c.count())
                    throw new NumberFormatException(String.format("Введённый индекс '%s' больше кол-ва семей в коллекции (%d)", i, c.count()));
                break;
            } catch (NumberFormatException e) {
                f("%s\n", e.getMessage());
            }
        }
        while (true) {
            ln("Введите 'male' если мальчик или 'female' если девочка");
            s = nextLine();
            if (Objects.equals(s, "male")) {
                s = "мальчика";
                break;
            }
            if (Objects.equals(s, "female")) {
                s = "девочки";
                break;
            }
        }
        Human child = logSub4(s);
        c.adoptChild(c.getFamilyById(i), child);
    }

    public static void sub7(FamilyController c) {
        while (true) {
            ln("Введите возраст старше которого надо удалить всех детей в семьях");
            try {
                int i = Integer.parseInt(nextLine());
                c.deleteAllChildrenOlderThen(i);
                break;
            } catch (NumberFormatException e) {
                f("%s\n", e.getMessage());
            }
        }
    }
}
