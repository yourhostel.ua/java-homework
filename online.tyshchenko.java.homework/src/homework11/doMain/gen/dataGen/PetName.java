package homework11.doMain.gen.dataGen;

public class PetName {

    public static final String[] nickname = new String[]
            {
                    "Bitsy",
                    "Piper",
                    "Ruby",
                    "Penny",
                    "Daisy",
                    "Pixi",
                    "Mindy",
                    "Iggy",
                    "Gwen",
                    "Helga",
                    "Cupid",
                    "Clover",
                    "Charity",
                    "Finn",
                    "Milo",
                    "Max",
                    "Loki",
                    "Peach",
                    "Ralph",
                    "Zac",
            };
}
