package homework11.doMain.gen;

import homework11.controller.FamilyController;
import homework11.model.baseClasses.Family;
import homework11.model.baseClasses.Human;
import homework11.model.baseClasses.Pet;
import homework11.model.derivativesFromHuman.Man;
import homework11.model.derivativesFromHuman.Woman;
import homework11.model.enumerations.DayOfWeek;
import homework11.model.enumerations.Species;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static homework11.doMain.gen.dataGen.Name.*;
import static homework11.doMain.gen.dataGen.PetName.nickname;
import static homework11.doMain.gen.dataGen.Schedule.shm;
import static homework11.view.Header.mainHead;
import static libs.Console.ln;
import static libs.MethodMath.random;

public class Gen {
    static Woman womanGen(int i, int j) {
        return new Woman(Timestamp.valueOf(LocalDate.now()
                .minusYears(random(i, j)).atStartOfDay()).getTime(),
                femaleName[random(0, 19)],
                surname[random(0, 19)]);
    }

    static Woman womanGen(String surname, int j) {
        return new Woman(Timestamp.valueOf(LocalDate.now()
                .minusYears(random(0, j)).atStartOfDay()).getTime(),
                femaleName[random(0, 19)],
                surname);
    }

    static Man manGen() {
        return new Man(Timestamp.valueOf(LocalDate.now()
                .minusYears(random(30, 60)).atStartOfDay()).getTime(),
                maleName[random(0, 19)],
                surname[random(0, 19)]);
    }

    static Man manGen(String surname, int j) {
        return new Man(Timestamp.valueOf(LocalDate.now()
                .minusYears(random(0, j)).atStartOfDay()).getTime()
                , maleName[random(0, 19)],
                surname);
    }

    static void childGen(Family f) {
        int r = random(0, 3);
        for (int j = 0; j < r; j++) {
            int age = f.getParentB().getAge()[0];
            if (random(0, 1) == 1) {
                f.addChild(manGen(f.getFamilyName(), age - 18));
            } else f.addChild(womanGen(f.getFamilyName(), age - 18));
        }
    }

    static Human[] pairGen() {
        Human man = manGen();
        man.setIq(random(70, 100));
        scheduleGen(man);
        Human woman = womanGen(man.getAge()[0] - 15, man.getAge()[0] + 2);
        woman.setIq(random(70, 100));
        scheduleGen(woman);
        return new Human[]{man, woman};
    }

    public static void gen(FamilyController controller) {
        for (int i = 0; i < 10; i++) {
            Human[] pair = pairGen();
            Human a = pair[0];
            Human b = pair[1];
            String randomSurnameFamily = random(0, 1) == 1 ?
                    a.getSurname() : random(0, 1) == 1 ?
                    b.getSurname() : a.getSurname() + "-" + b.getSurname();
            Family f = new Family(a, b, randomSurnameFamily);
            childGen(f);
            petsGen(f);
            controller.addFamily(f);
        }
        ln("Семьи успешно созданы!");
        mainHead(0);
    }

    static void petsGen(Family f) {
        int i = random(0, 3);
        Species[] species = Species.values();
        for (int j = 0; j < i; j++) {
            f.setPet(new Pet(nickname[random(0, 19)], species[random(0, 9)]) {
            });
        }
    }

    static void scheduleGen(Human h) {
        DayOfWeek[] week = DayOfWeek.values();
        Map<String, String> s = new HashMap<>();
        for (int j = 0; j < 6; j++) {
            if (random(0, 2) == 1) s.put(String.valueOf(week[random(j, 6)]), shm[random(0, 19)]);
        }
        h.setSchedule(s);
    }
}
