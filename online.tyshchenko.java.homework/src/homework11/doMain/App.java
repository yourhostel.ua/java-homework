package homework11.doMain;

import java.util.Objects;

import static homework11.doMain.menu.MainMenu.menu;
import static homework11.doMain.menu.MainMenu.valid;
import static homework11.view.Header.mainHead;
import static libs.Console.ln;
import static libs.Console.nextLine;

public class App {
    static public void run() {
        mainHead(0);
        while (true) {
            String cmd = nextLine();
            if (Objects.equals(cmd, "exit")) {
                ln("program completed");
                break;
            }
            if (valid(cmd)) {
                menu(cmd);
            }
        }
    }
}
