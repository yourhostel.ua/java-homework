package homework11.model.derivativesFromHuman;

import homework11.model.baseClasses.Human;

import java.util.Map;

import static libs.Console.log;

public final class Man extends Human {
    public Man(String year, String... a) {
        super(year, a);
    }

    public Man(long year, String... a) {
        super(year, a);
    }

    public Man(String year, int iq, Map<String, String> schedule, String... a) {
        super(year, iq, schedule, a);
    }

    public Man(String year, int iq, String... a) {
        super(year, iq, a);
    }

    public Man() {
    }

    public String repairCar() {
        return "чинит авто";
    }

    @Override
    public void greetPet() {
        log(String
                .format("Идем гулять %s?",
                        this.getFamily().getPet(0).getNickname()));
    }
}
