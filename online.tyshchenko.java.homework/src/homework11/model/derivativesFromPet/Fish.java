package homework11.model.derivativesFromPet;

import homework11.model.baseClasses.Pet;
import homework11.model.enumerations.Species;

import java.util.Set;

import static libs.Console.log;

public class Fish extends Pet {

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(Species species, String nickname) {
        super(nickname, species);
    }

    public Fish(int age, int trickLevel, Set<String> habits, Species species, String nickname) {
        super(age, trickLevel, habits, nickname, species);
    }

    @Override
    public void eat() {
        log(String.format("%s кушает \n", this.getNickname()));
    }

    @Override
    public void respond() {
        log(String.format("%s - это рыба \n", this.getNickname()));
    }
}
