package homework11.model.derivativesFromPet;

import homework11.model.baseClasses.Pet;
import homework11.model.derivativesFromPet.peculiarPropertiesPets.TrickyPets;
import homework11.model.enumerations.Species;

import java.util.Set;

import static libs.Console.log;

public class DomesticCat extends Pet implements TrickyPets {

    public DomesticCat(String nickname) {
        super(nickname);

    }

    public DomesticCat(Species species, String nickname) {
        super(nickname, species);
    }

    public DomesticCat(int age, int trickLevel, Set<String> habits, Species species, String nickname) {
        super(age, trickLevel, habits, nickname, species);
    }


    public void foul() {
        log("Нужно хорошо замести следы...\n");
    }

    @Override
    public void eat() {
        log("Я кушаю!\n");
    }

    @Override
    public void respond() {
        log(String
                .format("Привет, хозяин. Я - %s. Я соскучился!\n",
                        this.getNickname()));
    }
}
