package homework11.model.baseClasses;

import homework11.view.HelpObj;
import homework11.view.HelpObj.HelpObjHuman;

import java.time.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static homework11.helpers.Helpers.millisOf;
import static homework11.view.OverrideToString.*;
import static libs.Console.*;

public class Human {
    private long birthDate;
    private int iq;
    private Map<String, String> schedule = new HashMap<>();
    private String name;
    private String surname;
    private Family family;

    public Human(String birthDate, String... a) {
        this.birthDate = millisOf(birthDate);
        this.iq = 0;
        this.schedule = new HashMap<>();
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
    }

    public Human(long birthDate, String... a) {
        this.birthDate = birthDate;
        this.iq = 0;
        this.schedule = new HashMap<>();
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
    }

    public Human(String birthDate, int iq, Map<String, String> schedule, String... a) {
        this.birthDate = millisOf(birthDate);
        this.iq = iq;
        this.schedule.putAll(schedule);
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
    }

    public Human(String birthDate, int iq, String... a) {
        this.birthDate = millisOf(birthDate);
        this.iq = iq;
        this.schedule = new HashMap<>();
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
    }

    public Human() {
        this.birthDate = millisOf("01/01/0001");
        this.iq = 0;
        this.schedule = new HashMap<>();
        this.name = "name";
        this.surname = "surname";
        this.family = null;
    }

    public long getBirthDate() {
        return this.birthDate;
    }

    public String describeAge() {
        int[] a = getAge();
        return String
                .format("%s %s - Возраст: \n лет: %d; месяцев: %d; дней: %d;\n",
                        this.name,
                        this.surname,
                        a[0], a[1], a[2]);
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public int[] getAge() {
        LocalDate startDate = LocalDate.from(Instant
                .ofEpochMilli(birthDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime());
        LocalDate endDate = LocalDate.now();
        Period period = Period.between(startDate, endDate);
        return new int[]{period.getYears(), period.getMonths(), period.getDays()};
    }

    public int getIq() {
        return this.iq;
    }

    public Family getFamily() {
        return this.family;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = millisOf(birthDate);
    }

    public void setIq(int iq) {
        try {
            if (iq >= 0 && iq <= 100) this.iq = iq;
            else
                throw new Exception(String
                        .format(" Value not set! %d is not valid. \"iq\" should be from 0 to 100\n",
                                iq));
        } catch (Exception e) {
            err(e.getMessage());
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void feedPet() {
        f("Кормлю нашего питомца %s %s\n",
                this.family.getPet(0).getSpecies(),
                this.family.getPet(0).getNickname());
    }

    public void greetPet() {
        log(String
                .format("Привет, %s\n",
                        this.family.getPet(0).getNickname()));
    }

    public void describePet() {
        String trickLevel = this.family.getPet(0).getTrickLevel() < 50 ?
                "почти не хитрый" : "очень хитрый";
        String massage = String
                .format("У меня есть %s, ему %d лет, он %s\n",
                        this.family.getPet(0).getSpecies(),
                        this.family.getPet(0).getAge(),
                        trickLevel);
        log(massage);
    }

    public String prettyFormat() {
        return humanPrettyFormat(new HelpObj.hpf(this.name,
                this.surname, this.birthDate,
                this.iq, this.schedule));
    }

    @Override
    public String toString() {
        return overrideToStringHumans(
                new HelpObjHuman(
                        this.getName(), this.getSurname(), this.getBirthDate(),
                        this.getIq(), (HashMap<String, String>) this.schedule)
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        if (!Objects.equals(name, human.name)) return false;
        return Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        int result = 2000;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }
}
