package homework11.view;

import homework11.model.baseClasses.Human;
import homework11.model.baseClasses.Pet;
import homework11.model.enumerations.Species;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HelpObj {
    public record HelpObjPet(
            Species species,
            String nickname,
            int age,
            int trickLevel,
            Set<String> habits,
            boolean canFly,
            int numberOfLegs,
            boolean hasFur) {
    }

    public record HelpObjHuman(
            String name,
            String surname,
            long year,
            int iq,
            HashMap<String, String> schedule
    ) {
    }

    public record HelpObjFamily(
            String familyName,
            String parentA_Name,
            String parentA_Surname,
            String parentB_Name,
            String parentB_Surname,
            List<Human> children,
            List<Pet> pet
    ) {
    }

    public record fpf(
            String familyName,
            Human parentA,
            Human parentB,
            List<Human> children,
            List<Pet> pets
    ) {
    }

    public record ppf(
            Species species,
            String nickname,
            int age,
            int trickLevel,
            Set<String> habits
    ) {
    }

    public record hpf(
            String name,
            String surname,
            long birthDate,
            int iq,
            Map<String, String> schedule
    ) {
    }
}

