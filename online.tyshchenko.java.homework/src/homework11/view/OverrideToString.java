package homework11.view;

import homework11.model.baseClasses.Human;
import homework11.model.baseClasses.Pet;
import homework11.model.derivativesFromHuman.Man;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class OverrideToString {

    public static String overrideToStringPets(HelpObj.HelpObjPet a) {
        return String.format("""
                        %s{
                        \tnickname = '%s',\s
                        \tage = %d,\s
                        \ttrickLevel = %d,\s
                        \thabits = %s
                        \tspecies = {
                        \tcanFly = %s
                        \tnumberOfLegs = %s
                        \thasFur = %s
                        \t\t}
                        \t}
                        """,
                a.species(), a.nickname(), a.age(),
                a.trickLevel(), a.habits(),
                a.canFly(), a.numberOfLegs(), a.hasFur());
    }

    public static String petPrettyFormat(HelpObj.ppf a) {
        return String.format("{species='%s', nickname='%s',age=%s, trickLevel=%d, habits = %s}",
                a.species().name(), a.nickname(), a.age(), a.trickLevel(), a.habits());
    }

    public static String overrideToStringHumans(HelpObj.HelpObjHuman a) {
        return String.format("""
                        Human{\s
                        \tname = '%s',\s
                        \tsurname = '%s',\s
                        \tyear = %s,\s
                        \tiq = %d,\s
                        \tschedule = %s
                        }
                        """,
                a.name(), a.surname(), deploymentDate(a.year()), a.iq(),
                deploymentMap(a.schedule()));
    }

    public static String humanPrettyFormat(HelpObj.hpf a) {
        return String.format("{name=%s, surname=%s, birthDate=%s, iq=%d, schedule=%s}",
                a.name(), a.surname(), deploymentDate(a.birthDate()), a.iq(), a.schedule());
    }

    public static String overrideToStringFamily(HelpObj.HelpObjFamily a) {
        return String.format("""
                        \tFamily = %s{\s
                        \tparentA = %s %s,\s
                        \tparentB = %s %s,\s
                        \tchildren = %s,\s
                        \tpet = %s}
                        """,
                a.familyName(),
                a.parentA_Name(), a.parentA_Surname(),
                a.parentB_Name(), a.parentB_Surname(),
                a.children(), a.pet());
    }

    public static String familyPrettyFormat(HelpObj.fpf a) {
        return String.format("""
                        family: %s\s
                        \t\t\tmother: %s,\s
                        \t\t\tfather: %s,\s
                        \t\t\tchildren: \s
                        %s\s
                        \t\t\tpets:   \s
                        %s\s
                        """,
                a.familyName(),
                a.parentB().prettyFormat(), a.parentA().prettyFormat(),
                deploymentChildren(a.children()), deploymentPet(a.pets()));
    }

    static <K, V> String deploymentMap(Map<K, V> a) {
        StringBuilder b = new StringBuilder();
        for (Map.Entry<K, V> elem : a.entrySet()) {
            b.append(elem.getKey()).append(": ").append(elem.getValue()).append(", ");
        }
        return "[" + b + "]";
    }

    public static String deploymentDate(long a) {
        DateTimeFormatter g = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return Instant.ofEpochMilli(a).atZone(ZoneId.systemDefault()).toLocalDate().format(g);
    }

    static String deploymentChildren(List<Human> a) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < a.size(); i++) {
            String c = (a.get(i) instanceof Man) ? "boy" : "girl";
            b.append("\t\t\t\t\t").append(c).append(": ").append(a.get(i).prettyFormat());
            if (i < a.size() - 1) b.append("\n");
        }
        return String.format("%s", b);
    }

    static String deploymentPet(List<Pet> a) {
        StringBuilder b = new StringBuilder();
        for (Pet pet : a) b.append("\t\t\t\t\t").append(pet.prettyFormat()).append("\n");
        return String.format("%s", b);
    }
}
