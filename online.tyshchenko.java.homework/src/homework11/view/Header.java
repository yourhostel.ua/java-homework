package homework11.view;

import static libs.Console.f;
import static libs.Console.ln;

public class Header {

    public static final String GREEN = "\u001B[32m";
    public static final String CYAN = "\u001B[36m";
    public static final String RESET = "\u001B[0m";
    public static final String[] points = new String[]
            {
                    "- 1. Заполнить тестовыми данными",
                    "- 2. Отобразить весь список семей",
                    "- 3. Отобразить список семей кол-во членов > i",
                    "- 4. Отобразить список семей кол-во членов < i",
                    "- 5. Кол-во семей где кол-во членов = i",
                    "- 6. Создать новую семью",
                    "- 7. Удалить семью по индексу i",
                    "- 8. Редактировать семью по индексу",
                    "- 9. Удалить всех детей старше i",
                    "- 1. Родить ребенка в семье i ",
                    "- 2. Усыновить ребенка в семью i",
                    "- 3. Вернуться в главное меню",
            };

    public static void mainHead(int j) {
        String m = j == 9 ? "Edit  family" : "happy family";
        int k = 9, n = 8;
        if (j == 9) {
            k = 12;
            n = 11;
        }
        f(GREEN + "==========================================" + CYAN + " %S " + RESET + GREEN + "=============================================\n" + RESET, m);
        for (int i = j; i < k; i += 2) {
            if (i != n) f("  %-48s   %s\n", points[i], points[i + 1]);
            else f("  %s\n", points[i]);
        }
        ln(GREEN + "=====================================================================================================" + RESET);
    }
}
