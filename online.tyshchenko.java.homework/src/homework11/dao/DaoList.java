package homework11.dao;

import java.util.List;

public class DaoList<T> implements interfaceDao<T> {

    private final List<T> collection;

    public DaoList(List<T> o) {
        this.collection = o;
    }

    @Override
    public List<T> getAll() {
        return collection;
    }

    @Override
    public T getByIndex(int index) {
        return collection.get(index);
    }

    public T getByToObj(T o) {
        return collection.get(collection.indexOf(o));
    }

    @Override
    public boolean delete(int index) {
        T f = collection.remove(index);
        return f != null;
    }

    @Override
    public boolean delete(T o) {
        return collection.remove(o);
    }

    @Override
    public void save(T o) {
        if (collection.contains(o)) {
            collection.set(collection.indexOf(o), o);
        } else {
            collection.add(o);
        }
    }
}
