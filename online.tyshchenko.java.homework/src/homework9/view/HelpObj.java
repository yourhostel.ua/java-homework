package homework9.view;

import homework9.model.baseClasses.Human;
import homework9.model.baseClasses.Pet;
import homework9.model.enumerations.Species;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class HelpObj {
    public record HelpObjPet(
            Species species,
            String nickname,
            int age,
            int trickLevel,
            Set<String> habits,
            boolean canFly,
            int numberOfLegs,
            boolean hasFur) {
    }

    public record HelpObjHuman(
            String name,
            String surname,
            long year,
            int iq,
            HashMap<String, String> schedule
    ) {
    }

    public record HelpObjFamily(
            String familyName,
            String parentA_Name,
            String parentA_Surname,
            String parentB_Name,
            String parentB_Surname,
            List<Human> children,
            List<Pet> pet
    ) {
    }
}

