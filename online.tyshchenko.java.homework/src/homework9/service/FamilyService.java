package homework9.service;

import homework9.dao.DaoList;
import homework9.model.baseClasses.Family;
import homework9.model.baseClasses.Human;
import homework9.model.baseClasses.Pet;
import homework9.model.derivativesFromHuman.Man;
import homework9.model.derivativesFromHuman.Woman;
import libs.Console;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.function.Predicate;

import static libs.Console.f;
import static libs.MethodMath.random;

public class FamilyService {
    public static final String RED = "\u001B[31m";
    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";
    public DaoList<Family> service;

    public FamilyService(DaoList<Family> collectionDao) {
        this.service = collectionDao;
    }

    public List<Family> getAllFamilies() {
        return service.getAll();
    }

    public void displayAllFamilies() {
        service.getAll()
                .forEach(Console::ln);
    }

    public void getFamiliesBiggerThan(int count) {
        service.getAll()
                .stream()
                .filter(x -> x.countFamily() > count)
                .forEach(Console::ln);
    }

    public void getFamiliesLessThan(int count) {
        service.getAll()
                .stream()
                .filter(x -> x.countFamily() < count)
                .forEach(Console::ln);
    }

    public void countFamiliesWithMemberNumber(int count) {
        f("количество семей из %d чел - %d\n", count,
                service.getAll()
                        .stream()
                        .filter(x -> x.countFamily() == count)
                        .count());
    }

    public Family bornChild(Family f, String nameBoy, String nameGirl) {
        Predicate<Integer> boolInt = (s) -> s > 0;
        boolean b = boolInt.test(random(0, 1));
        if (b)
            f.addChild(new Man(Timestamp
                    .valueOf(LocalDate.now().atStartOfDay()).getTime(),
                    nameBoy,
                    f.getFamilyName())
            );
        if (!b)
            f.addChild(new Woman(Timestamp
                    .valueOf(LocalDate.now().atStartOfDay()).getTime(),
                    nameGirl,
                    f.getFamilyName())
            );

        service.save(f);

        f(BLUE + "У семьи %s %s - %s\n",
                f.getFamilyName(),
                b ? "родился мальчик" : "родилась девочка",
                b ? nameBoy : nameGirl
        );
        return service.getByToObj(f);
    }

    public void deleteAllChildrenOlderThen(int count) {
        long l = Timestamp.valueOf(LocalDate.now()
                .minusYears(count).atStartOfDay()).getTime();
        try {
            service.getAll()
                    .stream()
                    .filter(x -> x.getChildren().size() > 0)
                    .forEach(x -> x.getChildren().forEach(y -> {
                        if (y.getBirthDate() <= l) {
                            x.deleteChild(y);
                            deleteAllChildrenOlderThen(count);
                        }
                    }));
        } catch (ConcurrentModificationException ignored) {
        }
    }

    public void createNewFamily(Human parentA, Human parentB, String familyName) {
        service.save(new Family(parentA, parentB, familyName));
    }

    public Family addFamily(Family f) {
        service.save(f);
        return service.getByToObj(f);
    }

    public boolean deleteFamilyByIndex(int index) {
        f(RED + "семья %s удалена\n" + RESET,
                service.getByIndex(index).getFamilyName());
        return service.delete(index);
    }

    public boolean deleteFamilyByFamily(Family f) {
        f(RED + "семья %s удалена\n" + RESET, f.getFamilyName());
        return service.delete(f);
    }

    public Family adoptChild(Family f, Human h) {
        f.addChild(h);
        service.save(f);
        return service.getByToObj(f);
    }

    public int count() {//*
        return service.getAll().size();
    }

    public Family getFamilyById(int index) {
        return service.getByIndex(index);
    }

    public List<Pet> getPets(int i) {
        return service.getByIndex(i).getPets();
    }

    public void addPet(int index, Pet p) {
        service.getByIndex(index).setPet(p);
    }
}
