package homework9.model.baseClasses;

import homework9.view.HelpObj;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static homework9.view.OverrideToString.overrideToStringFamily;
import static libs.Console.log;

public class Family {
    public static final String RED = "\u001B[31m";
    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";

    private String familyName;
    private Human parentA;
    private Human parentB;
    private final List<Human> children;
    private final List<Pet> pets;

    public String getFamilyName() {
        return familyName;
    }

    public Family(Human parentA, Human parentB, String familyName) {
        this.parentA = parentA;
        this.parentB = parentB;
        this.familyName = familyName;
        this.children = new ArrayList<>();
        this.pets = new ArrayList<>();
        this.parentA.setFamily(this);
        this.parentB.setFamily(this);
    }

    public void setPet(Pet pet) {
        this.pets.add(pet);
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setParentA(Human parentA) {
        this.parentA = parentA;
    }

    public void setParentB(Human parentB) {
        this.parentB = parentB;
    }

    public void addChild(Human child) {
        child.setFamily(this);
        String massage = BLUE + String
                .format("ребенок %s %s добавлен в семью %s \n",
                        child.getName(),
                        child.getSurname(),
                        this.familyName) + RESET;
        this.children.add(child);
        log(massage);
    }

    public void deleteChild(Human child) {
        if (this.children.size() == 0) {
            log(RED + String
                    .format("в семье %s детей нет\n",
                            this.familyName) + RESET);
            return;
        }
        if (!this.children.remove(child)) {
            log(RED + String
                    .format("ребенка %s %s в семье %s нет\n",
                            child.getName(),
                            child.getSurname(),
                            this.familyName) + RESET);
            return;
        }
        log(RED + String
                .format("ребенок %s %s удален из семьи %s\n",
                        child.getName(),
                        child.getSurname(),
                        this.familyName) + RESET);
    }

    public boolean deleteChild(int index) {
        if (index < 0) {
            log(RED + String
                    .format("index = %d Индекс не может быть отрицательным\n",
                            index) + RESET);
            return false;
        }
        if (this.children.size() == 0) {
            log(RED + String
                    .format("в семье %s детей нет\n",
                            this.familyName) + RESET);
            return false;
        }
        Human human;
        try {
            human = this.children.remove(index);
        } catch (IndexOutOfBoundsException e) {
            log(RED + String
                    .format("в семье %s детей меньше чем задан индекс\n",
                            this.familyName) + RESET);
            return false;
        }
        log(RED + String
                .format("ребенок %s %s удален из семьи %s\n",
                        human.getName(),
                        human.getSurname(),
                        this.familyName) + RESET);
        return true;
    }

    public Human getParentA() {
        return parentA;
    }

    public Human getParentB() {
        return parentB;
    }

    public List<Human> getChildren() {
        return children;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public Pet getPet(int i) {
        return pets.get(i);
    }

    public int countFamily() {
        return this.children.size() + 2;
    }

    public void deleteFamily() {
        if (this.getChildren() != null) {
            for (Human child : this.getChildren()) {
                child.setFamily(null);
            }
        }
        this.getParentA().setFamily(null);
        this.getParentB().setFamily(null);
        this.setParentA(null);
        this.setParentB(null);
        this.setPet(null);
        this.setFamilyName(null);
        log(RED + "семья удалена \n" + RESET);
    }

    @Override
    public String toString() {
        String massage = "Был дан запрос о семье, но объект пуст, возможно данные были удалены";
        try {
            massage = overrideToStringFamily(new HelpObj.HelpObjFamily(this.familyName,
                    this.parentA.getName(), this.parentA.getSurname(),
                    this.parentB.getName(), this.parentB.getSurname(),
                    this.getChildren(), this.pets));
        } catch (Exception e) {
            log(RED + "Ошибка запроса данных о семье:\n" + RESET);
            log(RED + e.getMessage() + "\n" + RESET);
        }
        return massage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!Objects.equals(parentA, family.parentA)) return false;
        return Objects.equals(parentB, family.parentB);
    }

    @Override
    public int hashCode() {
        int result = parentA != null ? parentA.hashCode() : 0;
        result = 31 * result + (parentB != null ? parentB.hashCode() : 0);
        return result;
    }
}
