package homework9.model.enumerations;

public enum Species {
    DOG(false, 4, true),
    CAT(false, 4, true),
    TURTLE(false, 4, false),
    FISH(false, 0, false),
    PARROT(true, 2, false),
    SNAKE(false, 0, false),
    SPIDER(false, 8, false),
    DOMESTIC_CAT(false, 4, true),
    ROBO_CAT(false, 4, true),
    UNKNOWN;
    public boolean canFly;
    public boolean hasFur;
    public int numberOfLegs;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    Species() {
    }
}
