package homework6.views;

import homework6.baseClasses.Human;
import homework6.baseClasses.Pet;
import homework6.enumerations.Species;

public class HelpObj {
    public record HelpObjPet(
            Species species,
            String nickname,
            int age,
            int trickLevel,
            String[] habits,
            boolean canFly,
            int numberOfLegs,
            boolean hasFur) {
    }

    public record HelpObjHuman(
            String name,
            String surname,
            int year,
            int iq,
            String[][] schedule
    ) {
    }

    public record HelpObjFamily(
            String familyName,
            String parentA_Name,
            String parentA_Surname,
            String parentB_Name,
            String parentB_Surname,
            Human[] children,
            Pet pet
    ) {
    }
}

