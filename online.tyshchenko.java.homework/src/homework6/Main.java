package homework6;

import homework6.baseClasses.Family;
import homework6.baseClasses.Human;
import homework6.controllers.derivativesFromHuman.Man;
import homework6.controllers.derivativesFromHuman.Woman;
import homework6.controllers.derivativesFromPet.*;
import homework6.enumerations.Species;

import static homework5.DayOfWeek.*;
import static libs.Console.log;

public class Main {

    public static final String CYAN = "\u001B[36m";
    public static final String RESET = "\u001B[0m";

    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        log("totalMemory: " + runtime.totalMemory() / 1_000_000 + " Mbyte\n");
        log("freeMemory: " + runtime.freeMemory() / 1_000_000 + " Mbyte\n");
        for (int i = 0; i < 10; i++) {//у мене запускався гарбідж колектор з мільйона
            new Human();
        }
        //System.gc();

        log(CYAN + "create dogGlisy -------------------------------------------------------------------\n" + RESET);
        Dog dogGlisy = new Dog(3, 60, new String[]{"eat", "drink", "sleep"}, Species.DOG, "Glisy");
        dogGlisy.foul();
        dogGlisy.eat();
        dogGlisy.respond();
        log(dogGlisy + "\n");//toString()

        Man JuliusCaesar = new Man(60, 100, new String[][]{{SUNDAY.name(), "a"}, {MONDAY.name(), "b"}, {TUESDAY.name(), "c"}, {WEDNESDAY.name(), "d"}}, "Caesar", "Julius");

        Woman CorneliaCinna = new Woman(28, 100, new String[][]{{SUNDAY.name(), "task"}, {MONDAY.name(), "task_2"},}, "Cornelia", "Cinna");

        Human JuliaCinna = new Woman(10, "Julia", "Caesar");

        Human AugustusCaesar = new Man(12, "Augustus", "Caesar");
        AugustusCaesar.setYear(13);

        Human JessePinkman = new Man(27, "Jesse", "Pinkman");

        log(CYAN + "Family Caesar addChilds -----------------------------------------------------------\n" + RESET);
        Family familyCaesar = new Family(CorneliaCinna, JuliusCaesar, "Caesar");
        familyCaesar.setPet(dogGlisy);
        familyCaesar.addChild(JuliaCinna);
        familyCaesar.addChild(AugustusCaesar);
        log(familyCaesar + "\n");//toString()


        log(CYAN + "Julius Caesar ---------------------------------------------------------------------\n" + RESET);
        JuliusCaesar.feedPet();//Кормлю нашего питомца собака Glisy - викликаний на family
        log(JuliusCaesar + "\n");//toString()
        JuliusCaesar.greetPet();
        JuliusCaesar.describePet();
        log(CYAN + "Cornelia Cinna --------------------------------------------------------------------\n" + RESET);
        log(CorneliaCinna + "\n");//toString()
        log(CYAN + "Family Caesar children removed ----------------------------------------------------\n" + RESET);

        familyCaesar.setPet(new DomesticCat(3, 45, new String[]{"eat"}, Species.CAT, "Барсик"));

        familyCaesar.deleteChild(JessePinkman);//ребенка Jesse Pinkman в этой семье нет
        familyCaesar.deleteChild(AugustusCaesar);//ребенок Augustus Caesar удален из семьи Caesar
        familyCaesar.deleteChild(1);//в семье Caesar детей меньше чем задан индекс
        familyCaesar.deleteChild(0);//ребенок Julia Caesar удален из семьи Caesar
        familyCaesar.deleteChild(JessePinkman);//в семье Caesar детей нет
        log(familyCaesar + "\n");//toString()

        log(CYAN + "JuliusCaesar.getFamily().deleteFamily() -------------------------------------------\n" + RESET);
        JuliusCaesar.getFamily().deleteFamily();
        log(CYAN + "Надаємо запит на сім'ю familyCaesar -----------------------------------------------\n" + RESET);
        log(familyCaesar + "\n");//toString()//викличе помилку - Ошибка запроса данных о семье
        log(CYAN + "Пробуємо повернути сім'ю на JuliusCaesar ------------------------------------------\n" + RESET);
        log("JuliusCaesar.getFamily() = " + JuliusCaesar.getFamily() + "\n");

        log(CYAN + "Створюємо тестові сім'ї -----------------------------------------------------------\n" + RESET);
        log(CYAN + "сім'я StritskyBiel ----------------------------------------------------------------\n" + RESET);
        Man BobStritsky = new Man(27, "Bob", "Stritsky");
        Woman JessikaBiel = new Woman(27, 86, new String[][]{{SUNDAY.name(), "Do home work"}, {MONDAY.name(), "Go to courses"}, {TUESDAY.name(), "Go to explore the mountains"}}, "Jessika", "Biel");
        Family StritskyBiel = new Family(JessikaBiel, BobStritsky, "StritskyBiel");
        log(StritskyBiel + "\n");//toString()

        log(String.format("%s %s\n", BobStritsky.getName(), BobStritsky.repairCar()));//Bob чинит авто
        log(String.format("%s %s\n", JessikaBiel.getName(), JessikaBiel.makeup()));//Jessika краситься

        log(CYAN + "сім'я Besson ----------------------------------------------------------------------\n" + RESET);
        Human LucBesson = new Human();
        LucBesson.setYear(64);
        LucBesson.setName("Luc");
        LucBesson.setSurname("Besson");

        Human AnneParillaud = new Human(62, 90, new String[0][], "Anne", "Parillaud");
        Family Besson = new Family(AnneParillaud, LucBesson, "Besson");

        Besson.setParentA(null);

        Human GiuliettaBesson = new Human(36, "Giulietta", "Besson");
        Besson.addChild(GiuliettaBesson);
        log(Besson + "\n");//toString()
        log(CYAN + "TrickyPets ------------------------------------------------------------------------\n" + RESET);
        Dog Julya = new Dog(Species.DOG, "Julya");
        Julya.foul();
        DomesticCat Sonya = new DomesticCat(Species.DOMESTIC_CAT, "Sonya");
        Sonya.foul();
        Sonya.eat();
        log(CYAN + "OtherPets -------------------------------------------------------------------------\n" + RESET);
        Fish Lester = new Fish(Species.FISH, "Lester");
        //Lester.foul();//помилка тому що не імплементує інтерфейс хитрої тварини
        Lester.respond();//Lester - это рыба
        Lester.eat();//Lester кушает
        RoboCat MarsCat = new RoboCat(Species.ROBO_CAT, "MarsCat");
        //MarsCat.foul();//помилка тому що не імплементує інтерфейс хитрої тварини
        MarsCat.respond();
        MarsCat.eat();
        RoboCat robo = new RoboCat("MarsCat"); //Species.UNKNOWN
        Besson.setPet(robo);
        log(Besson + "\n");//pet = UNKNOWN
        log(CYAN + "Винятки та попередження -----------------------------------------------------------\n" + RESET);
    }
}
