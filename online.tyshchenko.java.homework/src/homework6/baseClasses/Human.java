package homework6.baseClasses;

import homework6.views.HelpObj.HelpObjHuman;

import java.util.Objects;

import static homework6.views.OverrideToString.overrideToStringHumans;
import static libs.Console.err;
import static libs.Console.log;

public class Human {

    private int year;
    //валідація для "final" тільки для прикладу iq - може збільшиться чи зменшиться :-)
    private final int iq;//0 > iq > 100 will return an exception
    private String[][] schedule;
    private String name;
    private String surname;
    private Family family;

    public Human(int year, String... a) {
        this.year = year;
        this.iq = 0;
        this.schedule = new String[0][];
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
    }

    public Human(int year, int iq, String[][] schedule, String... a) {
        this.year = year;
        //Якщо "final" присвоєння тільки в конструкторі встановлення через сеттер неможлива
        this.iq = setIq(iq);//але якщо потрібна валідація для "final" ми можемо зробити сеттер з "return"-ом
        //та викликати в конструкторі повертаючи значення під час побудови об'єкта
        this.schedule = schedule;
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
    }

    public Human() {
        this.year = 0;
        this.iq = 0;
        this.schedule = new String[0][];
        this.name = "name";
        this.surname = "surname";
        this.family = null;
    }

    public int getYear() {
        return this.year;
    }

    public int getIq() {
        return this.iq;
    }

    public Family getFamily() {
        return this.family;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int setIq(int iq) {
        try {
            if (iq >= 0 && iq <= 100) return iq;
            else
                throw new Exception(String.format(" Value not set! %d is not valid. \"iq\" should be from 0 to 100\n", iq));
        } catch (Exception e) {
            err(e.getMessage());
        }
        return 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void feedPet() {
        log("Кормлю нашего питомца " + this.family.getPet().getSpecies() + " " + this.family.getPet().getNickname() + "\n");
    }

    public void greetPet() {
        log(String.format("Привет, %s\n", this.family.getPet().getNickname()));
    }

    public void describePet() {
        String trickLevel = this.family.getPet().getTrickLevel() < 50 ? "почти не хитрый" : "очень хитрый";
        String massage = String.format("У меня есть %s, ему %d лет, он %s\n", this.family.getPet().getSpecies(), this.family.getPet().getAge(), trickLevel);
        log(massage);
    }

    @Override
    public String toString() {
        return overrideToStringHumans(
                new HelpObjHuman(
                        this.getName(), this.getSurname(), this.getYear(), this.getIq(), this.schedule)
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        if (!Objects.equals(name, human.name)) return false;
        return Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        int result = 2000;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
