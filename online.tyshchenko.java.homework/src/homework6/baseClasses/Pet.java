package homework6.baseClasses;

import homework6.enumerations.Species;

import java.util.Arrays;
import java.util.Objects;

import static libs.Console.err;

public abstract class Pet {

    public final String nickname;
    private String[] habits;
    private int age;
    private int trickLevel;

    public Pet(int age, int trickLevel, String[] habits, String nickname) {
        this.age = age;
        this.habits = habits;
        this.nickname = nickname;
        setTrickLevel(trickLevel); //Якщо не "final" можна встановити сеттером без присвоєння в конструкторі, установка через сеттер буде доступна.
    }

    public Pet(String nickname) {
        this.age = 0;
        this.habits = new String[0];
        this.nickname = nickname;
        this.trickLevel = 0;
    }

    public abstract void eat();

    public abstract void respond();

    public abstract Species getSpecies();

    public int getAge() {
        return this.age;
    }

    public String[] getHabits() {
        return this.habits;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void setTrickLevel(int trickLevel) {
        try {
            if (trickLevel >= 0 && trickLevel <= 100) this.trickLevel = trickLevel;
            else
                throw new Exception(String.format(" Value not set! %d is not valid. \"trickLevel\" should be from 0 to 100\n", trickLevel));
        } catch (Exception e) {
            err(e.getMessage());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        if (age != pet.age) return false;
        if (trickLevel != pet.trickLevel) return false;
        if (!Objects.equals(nickname, pet.nickname)) return false;
        return Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = nickname != null ? nickname.hashCode() : 0;
        result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
        result = 31 * result + trickLevel;
        return result;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
