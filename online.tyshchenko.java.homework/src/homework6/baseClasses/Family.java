package homework6.baseClasses;

import homework6.views.HelpObj;

import java.util.Objects;

import static homework6.views.OverrideToString.overrideToStringFamily;
import static libs.Console.err;
import static libs.Console.log;

public class Family {

    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";

    private String familyName;
    private Human parentA;
    private Human parentB;
    private Human[] children;
    private Pet pet;

    public Family(Human parentA, Human parentB, String familyName) {
        this.parentA = parentA;
        this.parentB = parentB;
        this.familyName = familyName;
        this.children = null;
        this.pet = null;
        this.parentA.setFamily(this);
        this.parentB.setFamily(this);
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setParentA(Human parentA) {
        this.parentA = parentA;
    }

    public void setParentB(Human parentB) {
        this.parentB = parentB;
    }

    public int countFamily() {
        return children.length + 2;
    }

    public void addChild(Human child) {
        child.setFamily(this);
        String massage = BLUE + String.format("ребенок %s %s добавлен в семью %s \n", child.getName(), child.getSurname(), this.familyName) + RESET;
        if (this.children == null) {
            this.children = new Human[]{child};
            log(massage);
        } else {
            int j = this.children.length;
            Human[] arr = new Human[j + 1];
            for (int i = 0; i < j + 1; i++) {
                if (i < j) arr[i] = this.children[i];
                else arr[i] = child;
            }
            this.children = arr;
            log(massage);
        }
    }

    public void deleteChild(Human child) {
        if (this.children == null) {
            err(String.format("в семье %s детей нет\n", this.familyName));
        } else {
            Human[] arr = new Human[this.children.length - 1];
            int j = this.children.length;
            for (int i = 0, k = 0; i < j; i++) {
                if (!this.children[i].equals(child)) {
                    k++;
                    if (k == j) {
                        err(String.format("ребенка %s %s в семье %s нет\n", child.getName(), child.getSurname(), this.familyName));
                        return;
                    }
                    arr[i] = this.children[i];
                }
            }

            if (this.children.length == 1) {
                this.children = null;
            } else {
                this.children = arr;
            }
            err(String.format("ребенок %s %s удален из семьи %s\n", child.getName(), child.getSurname(), this.familyName));
        }
    }

    public boolean deleteChild(int index) {
        boolean bool = false;
        Human human;
        if (this.children == null) {
            err(String.format("в семье %s детей нет\n", this.familyName));
        } else if (this.children.length < index + 1) {
            err(String.format("в семье %s детей меньше чем задан индекс\n", this.familyName));
        } else {
            human = this.children[index];
            Human[] arr = new Human[this.children.length - 1];
            int j = this.children.length - 1;
            for (int i = 0, k = 0; i < j; i++) {
                if (i != index) {
                    arr[k] = this.children[i];
                    k++;
                }
            }

            if (this.children.length == 1) {
                this.children = null;
            } else {
                this.children = arr;
            }
            err(String.format("ребенок %s %s удален из семьи %s\n", human.getName(), human.getSurname(), this.familyName));
            bool = true;
        }
        return bool;
    }

    public Human getParentA() {
        return parentA;
    }

    public Human getParentB() {
        return parentB;
    }

    public Human[] getChildren() {
        return children;
    }

    public String getFamilyName() {
        return familyName;
    }

    public Pet getPet() {
        return pet;
    }

    public void deleteFamily() {
        if (this.getChildren() != null) {
            for (Human child : this.getChildren()) {
                child.setFamily(null);//по можливості видаляємо всі посилання, щоб збирач сміття видалив об'єкт
            }
        }
        this.getParentA().setFamily(null);
        this.getParentB().setFamily(null);
        this.setParentA(null);
        this.setParentB(null);
        this.setPet(null);
        this.setFamilyName(null);
        log("семья удалена \n");
    }

    @Override
    public String toString() {
        String massage = "Был дан запрос о семье, но объект пуст, возможно данные были удалены";
        try {
            massage = overrideToStringFamily(new HelpObj.HelpObjFamily(this.familyName,
                    this.parentA.getName(), this.parentA.getSurname(),
                    this.parentB.getName(), this.parentB.getSurname(),
                    this.getChildren(), this.pet));
        } catch (Exception e) {
            err("Ошибка запроса данных о семье");
        }
        return massage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!Objects.equals(parentA, family.parentA)) return false;
        return Objects.equals(parentB, family.parentB);
    }

    @Override
    public int hashCode() {
        int result = parentA != null ? parentA.hashCode() : 0;
        result = 31 * result + (parentB != null ? parentB.hashCode() : 0);
        return result;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
