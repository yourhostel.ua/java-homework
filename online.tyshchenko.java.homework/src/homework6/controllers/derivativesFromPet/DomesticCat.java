package homework6.controllers.derivativesFromPet;

import homework6.baseClasses.Pet;
import homework6.enumerations.Species;
import homework6.views.*;
import homework6.models.TrickyPets;

import static libs.Console.log;

public class DomesticCat extends Pet implements TrickyPets {

    public final Species species;

    public DomesticCat(String nickname) {
        super(nickname);
        this.species = Species.UNKNOWN;
    }

    public DomesticCat(Species species, String nickname) {
        super(nickname);
        this.species = species;
    }

    public DomesticCat(int age, int trickLevel, String[] habits, Species species, String nickname) {
        super(age, trickLevel, habits, nickname);
        this.species = species;
    }

    public Species getSpecies() {
        return species;
    }

    public void foul() {
        log("Нужно хорошо замести следы...\n");
    }

    @Override
    public void eat() {
        log("Я кушаю!\n");
    }

    @Override
    public void respond() {
        log(String.format("Привет, хозяин. Я - %s. Я соскучился!\n", this.getNickname()));
    }

    @Override
    public String toString() {
        return OverrideToString.overrideToStringPets(
                new HelpObj.HelpObjPet(
                        this.species, this.getNickname(), this.getAge(),
                        this.getTrickLevel(), this.getHabits(), this.species.canFly,
                        this.species.numberOfLegs, this.species.hasFur));
    }
}
