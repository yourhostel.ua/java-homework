package homework6.controllers.derivativesFromHuman;

import homework6.baseClasses.Human;

import static libs.Console.log;

public final class Woman extends Human {
    public Woman(int year, String... a) {
        super(year, a);
    }

    public Woman(int year, int iq, String[][] schedule, String... a) {
        super(year, iq, schedule, a);
    }

    public Woman() {
    }

    public String makeup() {
        return "краситься";
    }

    @Override
    public void greetPet() {
        super.greetPet();
        log("Идем гулять?");
    }
}
