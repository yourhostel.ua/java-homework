package homework4;

import static libs.Console.log;

public class Main {
    public static final String CYAN = "\u001B[36m";
    public static final String RESET = "\u001B[0m";

    public static void main(String[] args) {
        log(CYAN + "create dogGlisy -------------------------------------------------------------------\n" + RESET);
        Pet dogGlisy = new Pet(3, 60, new String[]{"eat", "drink", "sleep"}, "собака", "Glisy");
        dogGlisy.foul();
        dogGlisy.eat();
        dogGlisy.respond();
        log(dogGlisy + "\n");//toString()

        Human JuliusCaesar = new Human(60, 100, new String[][]{{"task", "a"}, {"task", "b"}, {"task", "c"}, {"task", "d"}}, "Caesar", "Julius");

        Human CorneliaCinna = new Human(28, 100, new String[][]{{"day", "task"}, {"day_2", "task_2"},}, "Cornelia", "Cinna");

        Human JuliaCaesar = new Human(10, CorneliaCinna, JuliusCaesar, "Julia", "Caesar");

        Human AugustusCaesar = new Human(12, CorneliaCinna, JuliusCaesar, "Augustus", "Caesar");
        AugustusCaesar.setYear(13);

        Human JessePinkman = new Human(27, "Jesse", "Pinkman");

        log(CYAN + "Family Caesar addChilds -----------------------------------------------------------\n" + RESET);
        Family familyCaesar = new Family(CorneliaCinna, JuliusCaesar, "Caesar");
        familyCaesar.setPet(dogGlisy);
        familyCaesar.addChild(JuliaCaesar);
        familyCaesar.addChild(AugustusCaesar);
        log(familyCaesar + "\n");//toString()

        log(CYAN + "Jesse Pinkman ---------------------------------------------------------------------\n" + RESET);
        JessePinkman.setPet(110, 100, new String[]{"run"}, "черепаха", "Молния");
        JessePinkman.feedPet();//Кормлю своего питомца черепаха Молния - викликаний на Human


        log(CYAN + "Julius Caesar ---------------------------------------------------------------------\n" + RESET);
        JuliusCaesar.feedPet();//Кормлю нашего питомца собака Glisy - викликаний на family
        log(JuliusCaesar + "\n");//toString()
        JuliusCaesar.greetPet();
        JuliusCaesar.describePet();
        log(CYAN + "Cornelia Cinna --------------------------------------------------------------------\n" + RESET);
        log(CorneliaCinna + "\n");//toString()
        log(CYAN + "Family Caesar children removed ----------------------------------------------------\n" + RESET);

        familyCaesar.setPet(5, 78, new String[]{"eat"}, "кот", "Матроскин");//основний метод
        familyCaesar.setPet(new Pet(3, 45, new String[]{"eat"}, "кот", "Барсик"));//перевантажений метод

        familyCaesar.deleteChild(JessePinkman);//ребенка Jesse Pinkman в этой семье нет
        familyCaesar.deleteChild(JuliaCaesar);//ребенок Augustus Caesar удален из семьи Caesar
        familyCaesar.deleteChild(1);//в семье Caesar детей меньше чем задан индекс
        familyCaesar.deleteChild(0);//ребенок Julia Caesar удален из семьи Caesar
        familyCaesar.deleteChild(JessePinkman);//в семье Caesar детей нет
        log(familyCaesar + "\n");//toString()

        log(CYAN + "JuliusCaesar.getFamily().deleteFamily() -------------------------------------------\n" + RESET);
        JuliusCaesar.getFamily().deleteFamily();
        log(CYAN + "Надаємо запит на сім'ю familyCaesar -----------------------------------------------\n" + RESET);
        log(familyCaesar + "\n");//toString()//викличе помилку - Ошибка запроса данных о семье
        log(CYAN + "Пробуємо повернути сім'ю на JuliusCaesar ------------------------------------------\n" + RESET);
        log("JuliusCaesar.getFamily() = " + JuliusCaesar.getFamily() + "\n");

        log(CYAN + "Створюємо тестові сім'ї -----------------------------------------------------------\n" + RESET);
        log(CYAN + "сім'я StritskyBiel ----------------------------------------------------------------\n" + RESET);
        Human BobStritsky = new Human(27, "Bob", "Stritsky");
        Human JessikaBiel = new Human(27, 86, new String[][]{{"sunday", "Do home work"}, {"monday", "Go to courses"}, {"tuesday", "Go to explore the mountains"}}, "Jessika", "Biel");
        Family StritskyBiel = new Family(JessikaBiel, BobStritsky, "StritskyBiel");
        log(StritskyBiel + "\n");//toString()

        log(CYAN + "сім'я Besson ----------------------------------------------------------------------\n" + RESET);
        Human LucBesson = new Human();
        LucBesson.setYear(64);
        LucBesson.setName("Luc");
        LucBesson.setSurname("Besson");
        Human StefanParillaud = new Human(96, "Stefan", "Parillaud");
        Human MaryParillaud = new Human(85, "Mary", "Parillaud");
        Human AnneParillaud = new Human(62, 90, MaryParillaud, StefanParillaud, new String[0][], "Anne", "Parillaud");
        Family Besson = new Family(AnneParillaud, LucBesson, "Besson");
        Human GiuliettaBesson = new Human(36, "Giulietta", "Besson");
        Besson.addChild(GiuliettaBesson);
        log(Besson + "\n");//toString()
        log(CYAN + "Винятки та попередження -----------------------------------------------------------\n" + RESET);
    }
}
