package homework4;

import java.util.Arrays;

import static libs.Console.err;
import static libs.Console.log;

class Human {

    private int year;
    //валідація для "final" тільки для прикладу iq - може збільшиться чи зменшиться :-)
    private final int iq;//0 > iq > 100 will return an exception
    private Human mother;
    private Human father;
    private String[][] schedule;
    private String name;
    private String surname;
    private Family family;
    private Pet pet;

    Human(int year, String... a) {
        this.year = year;
        this.iq = 0;
        this.mother = new Human();
        this.father = new Human();
        this.schedule = new String[0][];
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
        this.pet = null;
    }

    Human(int year, Human mother, Human father, String... a) {
        this.year = year;
        this.iq = 0;
        this.schedule = new String[0][];
        this.mother = mother;
        this.father = father;
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
        this.pet = null;
    }

    Human(int year, int iq, String[][] schedule, String... a) {
        this.year = year;
        this.iq = setIq(iq);
        this.schedule = schedule;
        this.mother = new Human();
        this.father = new Human();
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
        this.pet = null;
    }

    Human(int year, int iq, Human mother, Human father, String[][] schedule, String... a) {
        this.year = year;
        //Якщо "final" присвоєння тільки в конструкторі встановлення через сеттер неможлива
        this.iq = setIq(iq);//але якщо потрібна валідація для "final" ми можемо зробити сеттер з "return"-ом
        //та викликати в конструкторі повертаючи значення під час побудови об'єкта
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
        this.pet = null;
    }

    Human() {
        this.year = 0;
        this.iq = 0;
        this.mother = null;
        this.father = null;
        this.schedule = new String[0][];
        this.name = "name";
        this.surname = "surname";
        this.family = null;
        this.pet = null;
    }


    int getYear() {
        return this.year;
    }

    int getIq() {
        return this.iq;
    }

    Human getMother() {
        return this.mother;
    }

    Human getFather() {
        return this.father;
    }

    String[][] getSchedule() {
        return this.schedule;
    }

    Family getFamily() {
        return this.family;
    }

    String getName() {
        return this.name;
    }

    String getSurname() {
        return this.surname;
    }

    void setYear(int year) {
        this.year = year;
    }

    int setIq(int iq) {
        try {
            if (iq >= 0 && iq <= 100) return iq;
            else
                throw new Exception(String.format(" Value not set! %d is not valid. \"iq\" should be from 0 to 100\n", iq));
        } catch (Exception e) {
            err(e.getMessage());
        }
        return 0;
    }

    void setMother(Human mother) {
        this.mother = mother;
    }

    void setMother(int year, int iq, Human mother, Human father, String[][] schedule, String... a) {
        this.mother = new Human(year, iq, mother, father, schedule, a[0], a[1]);
    }

    void setFather(Human father) {
        this.father = father;
    }

    void setFather(int year, int iq, Human mother, Human father, String[][] schedule, String... a) {
        this.father = new Human(year, iq, mother, father, schedule, a[0], a[1]);
    }

    void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    void setName(String name) {
        this.name = name;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    void setPet(Pet pet) {
        this.pet = pet;
    }

    void setPet(int age, int trickLevel, String[] habits, String... a) {
        this.pet = new Pet(age, trickLevel, habits, a[0], a[1]);
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    void feedPet() {//для прикладу метод передбачає наявність тварини у Human чи в сім'ї в якій може бути тварина
        String massageA = this.pet != null ? "Кормлю своего питомца " + this.pet.getSpecies() + " " + this.pet.getNickname() + "\n" : "Питомца нет";
        String massage = this.family != null ? "Кормлю нашего питомца " + this.family.getPet().getSpecies() + " " + this.family.getPet().getNickname() + "\n" : massageA;
        log(massage);
    }

    //нижні два методи можуть звертатися лише до поточної сім'ї
    //не переробляв, бо варіантів може бути багато і в завданні цього немає
    void greetPet() {
        log(String.format("Привет, %s\n", this.family.getPet().getNickname()));
    }

    void describePet() {
        String trickLevel = this.family.getPet().getTrickLevel() < 50 ? "почти не хитрый" : "очень хитрый";
        String massage = String.format("У меня есть %s, ему %d лет, он %s\n", this.family.getPet().getSpecies(), this.family.getPet().getAge(), trickLevel);
        log(massage);
    }

    @Override
    public String toString() {
        return String.format("""
                        Human{\s
                        \tname = '%s',\s
                        \tsurname = '%s',\s
                        \tyear = %d,\s
                        \tiq = %d,\s
                        \tschedule = %s}
                        """,
                this.getName(), this.getSurname(), this.getYear(), this.getIq(),
                Arrays.deepToString(this.schedule));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (!name.equals(human.name)) return false;
        return surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        return result;
    }
}
