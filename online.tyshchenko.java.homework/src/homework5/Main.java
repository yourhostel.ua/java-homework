package homework5;

import homework8.controller.FamilyController;

import static homework5.DayOfWeek.*;
import static homework5.Species.*;
import static libs.Console.log;

public class Main {

    public static final String CYAN = "\u001B[36m";
    public static final String RESET = "\u001B[0m";

    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        log("totalMemory: " + runtime.totalMemory() / 1_000_000 + " Mbyte\n");
        log("freeMemory: " + runtime.freeMemory() / 1_000_000 + " Mbyte\n");
        for (int i = 0; i < 10; i++) {//у мене запускався гарбідж колектор з мільйона
            new Human();
        }
        //System.gc();

        log(CYAN + "create dogGlisy -------------------------------------------------------------------\n" + RESET);
        Pet dogGlisy = new Pet(3, 60, new String[]{"eat", "drink", "sleep"}, DOG, "Glisy");
        dogGlisy.foul();
        dogGlisy.eat();
        dogGlisy.respond();
        log(dogGlisy + "\n");//toString()

        Human JuliusCaesar = new Human(60, 100, new String[][]{{SUNDAY.name(), "a"}, {MONDAY.name(), "b"}, {TUESDAY.name(), "c"}, {WEDNESDAY.name(), "d"}}, "Caesar", "Julius");

        Human CorneliaCinna = new Human(28, 100, new String[][]{{SUNDAY.name(), "task"}, {MONDAY.name(), "task_2"},}, "Cornelia", "Cinna");

        Human JuliaCinna = new Human(10, "Julia", "Caesar");

        Human AugustusCaesar = new Human(12, "Augustus", "Caesar");
        AugustusCaesar.setYear(13);

        Human JessePinkman = new Human(27, "Jesse", "Pinkman");

        log(CYAN + "Family Caesar addChilds -----------------------------------------------------------\n" + RESET);
        Family familyCaesar = new Family(CorneliaCinna, JuliusCaesar, "Caesar");
        familyCaesar.setPet(dogGlisy);
        familyCaesar.addChild(JuliaCinna);
        familyCaesar.addChild(AugustusCaesar);
        log(familyCaesar + "\n");//toString()

        log(CYAN + "Jesse Pinkman ---------------------------------------------------------------------\n" + RESET);

        log(CYAN + "Julius Caesar ---------------------------------------------------------------------\n" + RESET);
        JuliusCaesar.feedPet();//Кормлю нашего питомца собака Glisy - викликаний на family
        log(JuliusCaesar + "\n");//toString()
        JuliusCaesar.greetPet();
        JuliusCaesar.describePet();
        log(CYAN + "Cornelia Cinna --------------------------------------------------------------------\n" + RESET);
        log(CorneliaCinna + "\n");//toString()
        log(CYAN + "Family Caesar children removed ----------------------------------------------------\n" + RESET);

        familyCaesar.setPet(5, 78, new String[]{"eat"}, CAT, "Матроскин");//основний метод
        familyCaesar.setPet(new Pet(3, 45, new String[]{"eat"}, CAT, "Барсик"));//перевантажений метод

        familyCaesar.deleteChild(JessePinkman);//ребенка Jesse Pinkman в этой семье нет
        familyCaesar.deleteChild(AugustusCaesar);//ребенок Augustus Caesar удален из семьи Caesar
        familyCaesar.deleteChild(1);//в семье Caesar детей меньше чем задан индекс
        familyCaesar.deleteChild(0);//ребенок Julia Caesar удален из семьи Caesar
        familyCaesar.deleteChild(JessePinkman);//в семье Caesar детей нет
        log(familyCaesar + "\n");//toString()

        log(CYAN + "JuliusCaesar.getFamily().deleteFamily() -------------------------------------------\n" + RESET);
        JuliusCaesar.getFamily().deleteFamily();
        log(CYAN + "Надаємо запит на сім'ю familyCaesar -----------------------------------------------\n" + RESET);
        log(familyCaesar + "\n");//toString()//викличе помилку - Ошибка запроса данных о семье
        log(CYAN + "Пробуємо повернути сім'ю на JuliusCaesar ------------------------------------------\n" + RESET);
        log("JuliusCaesar.getFamily() = " + JuliusCaesar.getFamily() + "\n");

        log(CYAN + "Створюємо тестові сім'ї -----------------------------------------------------------\n" + RESET);
        log(CYAN + "сім'я StritskyBiel ----------------------------------------------------------------\n" + RESET);
        Human BobStritsky = new Human(27, "Bob", "Stritsky");
        Human JessikaBiel = new Human(27, 86, new String[][]{{SUNDAY.name(), "Do home work"}, {MONDAY.name(), "Go to courses"}, {TUESDAY.name(), "Go to explore the mountains"}}, "Jessika", "Biel");
        Family StritskyBiel = new Family(JessikaBiel, BobStritsky, "StritskyBiel");
        log(StritskyBiel + "\n");//toString()
        log(CYAN + "сім'я Besson ----------------------------------------------------------------------\n" + RESET);
        Human LucBesson = new Human();
        LucBesson.setYear(64);
        LucBesson.setName("Luc");
        LucBesson.setSurname("Besson");
        Human AnneParillaud = new Human(62, 90, new String[0][], "Anne", "Parillaud");
        Family Besson = new Family(AnneParillaud, LucBesson, "Besson");
        Human GiuliettaBesson = new Human(36, "Giulietta", "Besson");
        Besson.addChild(GiuliettaBesson);
        log(Besson + "\n");//toString()
        log(CYAN + "Винятки та попередження -----------------------------------------------------------\n" + RESET);
    }
}
