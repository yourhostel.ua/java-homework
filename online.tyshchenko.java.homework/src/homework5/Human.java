package homework5;

import java.util.Arrays;
import java.util.Objects;

import static libs.Console.err;
import static libs.Console.log;

public class Human {

    private int year;
    //валідація для "final" тільки для прикладу iq - може збільшиться чи зменшиться :-)
    private final int iq;//0 > iq > 100 will return an exception
    private String[][] schedule;
    private String name;
    private String surname;
    private Family family;

    public Human(int year, String... a) {
        this.year = year;
        this.iq = 0;
        this.schedule = new String[0][];
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
    }

    public Human(int year, int iq, String[][] schedule, String... a) {
        this.year = year;
        //Якщо "final" присвоєння тільки в конструкторі встановлення через сеттер неможлива
        this.iq = setIq(iq);//але якщо потрібна валідація для "final" ми можемо зробити сеттер з "return"-ом
        //та викликати в конструкторі повертаючи значення під час побудови об'єкта
        this.schedule = schedule;
        this.name = a[0];
        this.surname = a[1];
        this.family = null;
    }

    public Human() {
        this.year = 0;
        this.iq = 0;
        this.schedule = new String[0][];
        this.name = "name";
        this.surname = "surname";
        this.family = null;
    }

    int getYear() {
        return this.year;
    }

    int getIq() {
        return this.iq;
    }

    Family getFamily() {
        return this.family;
    }

    String getName() {
        return this.name;
    }

    String getSurname() {
        return this.surname;
    }

    void setYear(int year) {
        this.year = year;
    }

    int setIq(int iq) {
        try {
            if (iq >= 0 && iq <= 100) return iq;
            else
                throw new Exception(String.format(" Value not set! %d is not valid. \"iq\" should be from 0 to 100\n", iq));
        } catch (Exception e) {
            err(e.getMessage());
        }
        return 0;
    }

    void setName(String name) {
        this.name = name;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    void feedPet() {//для прикладу метод передбачає наявність тварини у Human чи в сім'ї в якій може бути тварина
        String massageA = this.family.getPet() != null ? "Кормлю своего питомца " + this.family.getPet().getSpecies() + " " + this.family.getPet().getNickname() + "\n" : "Питомца нет";
        String massage = this.family != null ? "Кормлю нашего питомца " + this.family.getPet().getSpecies() + " " + this.family.getPet().getNickname() + "\n" : massageA;
        log(massage);
    }

    //нижні два методи можуть звертатися лише до поточної сім'ї
    //не переробляв, бо варіантів може бути багато і в завданні цього немає
    void greetPet() {
        log(String.format("Привет, %s\n", this.family.getPet().getNickname()));
    }

    void describePet() {
        String trickLevel = this.family.getPet().getTrickLevel() < 50 ? "почти не хитрый" : "очень хитрый";
        String massage = String.format("У меня есть %s, ему %d лет, он %s\n", this.family.getPet().getSpecies(), this.family.getPet().getAge(), trickLevel);
        log(massage);
    }

    @Override
    public String toString() {
        return String.format("""
                        Human{\s
                        \tname = '%s',\s
                        \tsurname = '%s',\s
                        \tyear = %d,\s
                        \tiq = %d,\s
                        \tschedule = %s
                        }
                        """,
                this.getName(), this.getSurname(), this.getYear(), this.getIq(),
                Arrays.deepToString(this.schedule));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        if (!Objects.equals(name, human.name)) return false;
        return Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        int result = 2000;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
