package homework5;

public enum Species {
    DOG(false, 4, true),
    CAT(false, 4, true),
    TURTLE(false, 4, false),
    FISH(false, 0, false),
    PARROT(true, 2, false),
    SNAKE(false, 0, false),
    SPIDER(false, 8, false);
    final boolean canFly;
    final boolean hasFur;
    final int numberOfLegs;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}
