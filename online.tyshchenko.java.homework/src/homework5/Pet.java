package homework5;

import java.util.Arrays;
import java.util.Objects;

import static libs.Console.err;
import static libs.Console.log;

public class Pet {

    private final Species species;//немає сенсу в сетері
    private final String nickname;//також немає сенсу в сетері
    private String[] habits;
    private int age;
    private int trickLevel;//0 - 100 will return an exception

    Pet(Species species, String nickname) {
        this.age = 0;
        this.habits = new String[0];
        this.species = species;
        this.nickname = nickname;
        this.trickLevel = 0;
    }

    Pet(int age, int trickLevel, String[] habits, Species species, String nickname) {
        this.age = age;
        this.habits = habits;
        this.species = species;
        this.nickname = nickname;
        setTrickLevel(trickLevel); //Якщо не "final" можна встановити сеттером без присвоєння в конструкторі, установка через сеттер буде доступна.
    }

    void eat() {
        log("Я кушаю!\n");
    }

    void respond() {
        log(String.format("Привет, хозяин. Я - %s. Я соскучился!\n", this.nickname));
    }

    void foul() {
        log("Нужно хорошо замести следы...\n");
    }

    int getAge() {
        return this.age;
    }

    String[] getHabits() {
        return this.habits;
    }

    Species getSpecies() {
        return this.species;
    }

    String getNickname() {
        return this.nickname;
    }

    int getTrickLevel() {
        return this.trickLevel;
    }

    void setAge(int age) {
        this.age = age;
    }

    void setHabits(String[] habits) {
        this.habits = habits;
    }

    void setTrickLevel(int trickLevel) {
        try {
            if (trickLevel >= 0 && trickLevel <= 100) this.trickLevel = trickLevel;
            else
                throw new Exception(String.format(" Value not set! %d is not valid. \"trickLevel\" should be from 0 to 100\n", trickLevel));
        } catch (Exception e) {
            err(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return String.format("""
                        %s{
                        \tnickname = '%s',\s
                        \tage = %d,\s
                        \ttrickLevel = %d,\s
                        \thabits = %s
                        \tspecies = {
                        \tcanFly = %s
                        \tnumberOfLegs = %s
                        \thasFur = %s
                        \t\t}
                        \t}
                        """,
                this.getSpecies(), this.getNickname(), this.getAge(),
                this.getTrickLevel(), Arrays.toString(this.habits),
                this.species.canFly, this.species.numberOfLegs, this.species.hasFur);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        if (age != pet.age) return false;
        if (trickLevel != pet.trickLevel) return false;
        if (!Objects.equals(nickname, pet.nickname)) return false;
        return Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = species != null ? species.hashCode() : 0;
        result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
        result = 31 * result + trickLevel;
        return result;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
