package homework1;

import libs.Console;
import libs.MethodMath;

import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        while (true) {
            Console.log("Let the game begin!\n");
            String[][] history = new History().getArray();

            byte random = (byte) MethodMath.random(0, 100);
            int date = Integer.parseInt(history[random][0]);
            String st = history[random][1];
            Console.log("Події XVIII століття - дата може бути від 1701 до 1800 року\n");
            Console.log(String.format("У якому році сталася подія: %s\n", st));

            //цей рядок для тесту, треба закоментувати та вгадувати дату за описаними подіями----------
            Console.log(String.format("подія сталася: %s (тільки для тесту - закоментувати)\n", date));
            //-----------------------------------------------------------------------------------------

            while (true) {
                try {
                    String in = Console.nextLine();
                    int userDate = Integer.parseInt(in);

                    if (userDate == date) {
                        Console.log("Правильно! \n");
                        Console.log(String.format("У %d році відбувалися такі події: \n", date));

                        for (byte k = 0, l = 0, m = 1; k < history.length; k++) {
                            if (date == Integer.parseInt(history[k][l])) {
                                Console.log(String.format("%d : %s\n", m, history[k][l + 1]));
                                m++;
                            }
                        }

                        Console.log("Продовжити? (y/n)\n");
                        String y = Console.nextLine();
                        if (Objects.equals(y, "y")) break;
                        else return;

                    } else if (userDate > date) {
                        Console.err("Не вірно, це було раніше\n");
                    } else {
                        Console.err("Не вірно, це було пізніше\n");
                    }

                } catch (NumberFormatException err) {
                    Console.err(String.format("%s не є число\n", err.getMessage()));
                }
            }
        }
    }
}




