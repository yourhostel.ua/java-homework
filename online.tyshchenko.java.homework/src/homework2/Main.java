package homework2;

import libs.Console;
import libs.MethodMath;

import java.util.Objects;

public class Main {
    public static final String RESET = "\u001B[0m";
    //public static final String BLACK = "\u001B[30m";
    public static final String RED = "\u001B[31m";
    //public static final String GREEN = "\u001B[32m";
    public static final String YELLOW = "\u001B[33m";
    //public static final String BLUE = "\u001B[34m";
    public static final String PURPLE = "\u001B[35m";
    public static final String CYAN = "\u001B[36m";
    //public static final String WHITE = "\u001B[37m";

    public static void main(String[] args) {
        String[][] field = new String[6][6];
        int[] shot;
        int x, y, R_X = MethodMath.random(1, 5), R_Y = MethodMath.random(1, 5);
        //цей рядок для тесту, треба закоментувати-------------------
        Console.log(String.format("x: %d, y: %d - тест: рандомні координати цілі.\n", R_X, R_Y));
        //-----------------------------------------------------------
        drawing(field);
        while (true) {
            do {
                Console.log(CYAN + "\nВведіть координати цілі - два числа `x` та `y` (от 1-го до 5) через пробіл.\n" + RESET);
                shot = check(Console.nextLine());
                x = shot[0];
                y = shot[1];
            } while (shot[2] == 1);
            if (x == R_X && y == R_Y) {
                total(field, x, y, "X");
                break;
            } else {
                total(field, x, y, "*");
            }
        }
    }

    public static void setField(String[][] field) {
        Console.log("x y →\n↓ ");
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (i == 0) {
                    field[i][j] = String.format("| %s ", j);
                } else if (j == 0) {
                    field[i][j] = String.format("  | %s ", i);
                } else if (Objects.equals(field[i][j], "*") || Objects.equals(field[i][j], "| * ")) {
                    field[i][j] = "| * ";
                } else if (Objects.equals(field[i][j], "X")) {
                    field[i][j] = "| X ";
                } else field[i][j] = "| - ";
            }
        }
    }

    public static void drawing(String[][] field) {
        setField(field);
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (Objects.equals(field[i][j], "| * ")) {
                    Console.log(field[i][j].split(" ")[0] + " " + PURPLE + field[i][j].split(" ")[1] + " " + RESET);
                    continue;
                }
                if (Objects.equals(field[i][j], "| X ")) {
                    Console.log(field[i][j].split(" ")[0] + " " + RED + field[i][j].split(" ")[1] + " " + RESET);
                    continue;
                }
                Console.log(field[i][j]);
            }
            Console.log("\n");
        }
    }

    public static int[] check(String shot) {
        int[] a = new int[3];
        String mas = "не входить в діапазон від 1 до 5";
        try {
            a[0] = Integer.parseInt(shot.split(" ")[0]);
            a[1] = Integer.parseInt(shot.split(" ")[1]);

            if (1 > a[0] || a[0] > 5) {
                throw new Exception(String.format("Значення `%s` %s", a[0], mas));
            }
            if (1 > a[1] || a[1] > 5) {
                throw new Exception(String.format("Значення `%s` %s", a[1], mas));
            }
        } catch (Exception e) {
            a[2] = 1;
            Console.err(String.format("Невалідне значення: %s \n", e.getMessage()));
        }
        return a;
    }

    public static void total(String[][] field, int x, int y, String mark) {
        String mas = Objects.equals(mark, "X") ? YELLOW + "Є влучення ви перемогли!\n" + RESET
                : overshoot(x, y);
        Console.log(mas);
        field[x][y] = mark;
        drawing(field);
    }

    public static String overshoot(int x, int y) {
        Console.log(RED + "Ви не влучили! " + RESET);
        Console.log(String.format("Ваш постріл => x: %d, y: %d\n", x, y));
        return "";
    }
}