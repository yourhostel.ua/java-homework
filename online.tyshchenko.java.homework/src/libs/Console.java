package libs;

import java.util.Scanner;

public class Console {
    public static void log(String line) {
        System.out.print(line);
    }

    public static <T> void ln(T line) {
        System.out.println(line);
    }

    public static void f(String s, Object... o) {
        System.out.printf(s, o);
    }

    public static void err(String line) {
        System.err.print(line);
    }

    public static String nextLine() {
        return new Scanner(System.in).nextLine();
    }

}

