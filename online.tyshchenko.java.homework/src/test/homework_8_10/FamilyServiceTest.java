package test.homework_8_10;

import homework8.controller.FamilyController;
import homework8.dao.CollectionFamilyDao;
import homework8.model.baseClasses.Family;
import homework8.model.baseClasses.Human;
import homework8.service.FamilyService;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Objects;

import static libs.Console.log;
import static org.junit.Assert.*;

public class FamilyServiceTest {
    FamilyService s = new FamilyService(new CollectionFamilyDao(new ArrayList<>()));
    public FamilyController c = new FamilyController(s);
    Human child1 = new Human(3, "child1", "child1");
    Human child2 = new Human(2, "child2", "child2");
    Human child3 = new Human(7, "child3", "child3");
    Human parentA = new Human(25, "parentA", "parentA");
    Human parentB = new Human(30, "parentB", "parentB");
    Human parentC = new Human(41, "parentC", "parentC");
    Human parentD = new Human(38, "parentD", "parentD");

    @Test //TODO test createNewFamily, getFamilyById
    public void test1() {
        c.service.createNewFamily(parentA, parentB, "newFamily");
        assertEquals(c.service.getFamilyById(0).getFamilyName(), "newFamily");
    }

    @Test //TODO test addFamily, count
    public void test2() {
        Family family2 = new Family(parentA, parentB, "family1");
        c.service.addFamily(family2);

        assertEquals(c.service.getFamilyById(0).getFamilyName(), "family1");
        assertEquals(c.service.count(), 1);
    }

    @Test //TODO test deleteAllChildrenOlderThen
    public void test3() {
        Family family1 = new Family(parentA, parentB, "family1");
        Family family2 = new Family(parentC, parentD, "family2");
        c.service.addFamily(family1);
        c.service.addFamily(family2);
        //log(c.service.count() + " - семей\n");
        c.service.getFamilyById(0).addChild(child1);
        c.service.getFamilyById(1).addChild(child2);
        c.service.getFamilyById(1).addChild(child3);
        boolean bool = c.service.getFamilyById(1).getChildren().size() == 2;
        assertTrue(bool);

        c.service.deleteAllChildrenOlderThen(4);

        boolean bool2 = c.service.getFamilyById(0).getChildren().size() == 1;
        assertTrue(bool2);
        boolean bool3 = c.service.getFamilyById(1).getChildren().size() == 1;
        assertTrue(bool3);
    }

    @Test //TODO test deleteFamilyByIndex
    public void test4() {
        Family family1 = new Family(parentA, parentB, "family1");
        Family family2 = new Family(parentC, parentD, "family2");
        c.service.addFamily(family1);
        c.service.addFamily(family2);
        c.service.deleteFamilyByIndex(0);
        assertEquals(c.service.count(), 1);
    }

    @Test //TODO test adoptChild
    public void test5() {
        Family family1 = new Family(parentA, parentB, "family1");
        c.service.addFamily(family1);
        c.service.adoptChild(family1, child1);
        assertEquals(c.service.getFamilyById(0).getChildren().size(), 1);
        assertEquals(c.service.getFamilyById(0).getChildren().get(0).getName(), "child1");
    }


    @Test //TODO bornChild
    public void test6() {
        Family family1 = new Family(parentA, parentB, "family1");
        c.service.addFamily(family1);
        boolean b, b1 = false, b2 = false;
        int i = 0;
        assertEquals(c.service.getFamilyById(0).getChildren().size(), 0);
        while (true) {
            c.service.bornChild(family1, "Boy", "Girl");
            if (Objects.equals(c.service.getFamilyById(0).getChildren().get(i).getName(), "Boy")) {
                b1 = true;
            } else {
                b2 = true;
            }
            if (b1 && b2 || i >= 10) { //народжуємо поки обидва варіанти не спрацюють або не дійдемо до 10
                b = true;
                break;
            }
            i++;
        }
        assertTrue(b);
    }

    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Test //TODO test countFamiliesWithMemberNumber
    public void test7() {
        Family family1 = new Family(parentA, parentB, "family1");
        Family family2 = new Family(parentC, parentD, "family2");
        c.service.addFamily(family1);
        c.service.addFamily(family2);
        c.service.getFamilyById(0).addChild(child1);
        c.service.getFamilyById(0).addChild(child2);
        c.service.getFamilyById(1).addChild(child3);
        c.service.adoptChild(family1, child1);
        System.setOut(new PrintStream(output));
        c.service.countFamiliesWithMemberNumber(3);
        assertEquals(output.toString(), "количество семей из 3 чел - 1\n");
        System.setOut(null);
        assertEquals(c.service.getFamilyById(0).getChildren().get(0).getName(), "child1");
    }
}

