package test.homework7;

import homework7.baseClasses.Family;
import homework7.baseClasses.Human;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AllTest {
    Human child1 = new Human(10, "child1", "child1");
    Human child2 = new Human(10, "child2", "child2");
    Human child3 = new Human(10, "child3", "child3");
    Human parentA = new Human(10, "parentA", "parentA");
    Human parentB = new Human(10, "parentB", "parentB");


    @Test
    public void deleteChildByIndex() {
        Family family1 = new Family(parentA, parentB, "family1");
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);

        assertEquals(family1.getChildren().size(), 3);//перевірили що додається
        boolean bool = family1.deleteChild(2);
        assertTrue(bool);//перевірили, що після видалення метод deleteChild повернув True
        bool = family1.deleteChild(3);
        assertFalse(bool);//перевірили, що після видалення метод deleteChild повернув False
    }

    @Test
    public void deleteChildByObject() {
        Family family2 = new Family(parentA, parentB, "family1");
        family2.addChild(child1);
        family2.addChild(child2);
        family2.addChild(child3);

        assertEquals(family2.getChildren().size(), 3);//перевірили що додається
        family2.deleteChild(child3);
        assertEquals(family2.getChildren().size(), 2);//перевірили, що після видалення масив став менше
        family2.deleteChild(child3);
        assertEquals(family2.getChildren().size(), 2);//перевірили що не видаляє якщо в масиві об'єкт не знайдено
    }

    @Test
    public void addChild() {
        Family family3 = new Family(parentA, parentB, "family1");
        family3.addChild(child1);
        assertEquals(family3.getChildren().size(), 1);//перевірили що додається
        assertEquals(family3.getChildren().get(0), child1);//перевірили, що саме той об'єкт
    }

    @Test
    public void countFamily() {
        Family family4 = new Family(parentA, parentB, "family1");
        family4.addChild(child1);
        assertEquals(family4.countFamily(), 3);//перевірили кількість людей у сім'ї
    }
}