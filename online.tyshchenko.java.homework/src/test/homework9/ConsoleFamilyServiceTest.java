package test.homework9;

import homework9.controller.FamilyController;
import homework9.dao.DaoList;
import homework9.model.baseClasses.Family;
import homework9.model.baseClasses.Human;
import homework9.model.baseClasses.Pet;
import homework9.model.derivativesFromPet.Dog;
import homework9.model.derivativesFromPet.DomesticCat;
import homework9.model.derivativesFromPet.RoboCat;
import homework9.model.enumerations.Species;
import homework9.service.FamilyService;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;

public class ConsoleFamilyServiceTest {
    private final ByteArrayOutputStream output = new ByteArrayOutputStream();
    FamilyService s = new FamilyService(new DaoList<>(new ArrayList<>()));
    public FamilyController c = new FamilyController(s);

    Pet pet1 = new Dog("pet1");
    Pet pet2 = new RoboCat("pet2");
    Pet pet3 = new DomesticCat(Species.DOMESTIC_CAT, "pet3");

    @Test //TODO test getFamiliesLessThan
    public void test8() {
        Family family1 = new Family(new Human(25, "parent", "parent"), new Human(30, "parentB", "parentB"), "family1");
        Family family2 = new Family(new Human(41, "parentC", "parentC"), new Human(38, "parentD", "parentD"), "family2");
        c.service.addFamily(family1);
        c.service.addFamily(family2);

        System.setOut(new PrintStream(output));
        c.service.getFamiliesLessThan(4);
        assertLinesMatch(output.toString().lines(), ("""
                \tFamily = family1{\s
                \tparentA = parent parent,\s
                \tparentB = parentB parentB,\s
                \tchildren = [],\s
                \tpet = []}

                \tFamily = family2{\s
                \tparentA = parentC parentC,\s
                \tparentB = parentD parentD,\s
                \tchildren = [],\s
                \tpet = []}

                """).lines());
        System.setOut(null);
    }

    @Test //TODO test addPet, getPets
    public void test9() {
        Family family = new Family(new Human(25, "parentA", "parentA"), new Human(30, "parentB", "parentB"), "family1");
        c.service.addFamily(family);
        c.service.addPet(0, pet1);
        c.service.addPet(0, pet2);
        c.service.addPet(0, pet3);
        assertEquals(c.service.getPets(0).size(), 3);
        assertLinesMatch((c.service.getPets(0) + "").lines(), ("""
                [UNKNOWN{
                \tnickname = 'pet1',\s
                \tage = 0,\s
                \ttrickLevel = 0,\s
                \thabits = []
                \tspecies = {
                \tcanFly = false
                \tnumberOfLegs = 0
                \thasFur = false
                \t\t}
                \t}
                , UNKNOWN{
                \tnickname = 'pet2',\s
                \tage = 0,\s
                \ttrickLevel = 0,\s
                \thabits = []
                \tspecies = {
                \tcanFly = false
                \tnumberOfLegs = 0
                \thasFur = false
                \t\t}
                \t}
                , DOMESTIC_CAT{
                \tnickname = 'pet3',\s
                \tage = 0,\s
                \ttrickLevel = 0,\s
                \thabits = []
                \tspecies = {
                \tcanFly = false
                \tnumberOfLegs = 4
                \thasFur = true
                \t\t}
                \t}
                ]""").lines());
    }

    @Test //TODO getFamiliesBiggerThan
    public void test10() {
        Family family3 = new Family(new Human(25, "parentA", "parentA"), new Human(30, "parentB", "parentB"), "family1");
        Family family4 = new Family(new Human(41, "parentC", "parentC"), new Human(38, "parentD", "parentD"), "family2");
        c.service.addFamily(family3);
        c.service.addFamily(family4);
        c.service.getFamilyById(0).addChild(new Human(5, "child5", "child5"));
        c.service.getFamilyById(0).addChild(new Human(8, "child6", "child6"));
        c.service.getFamilyById(1).addChild(new Human(12, "child7", "child7"));
        Human child8 = new Human(6, "child8", "child8");
        c.service.adoptChild(family3, child8);

        System.setOut(new PrintStream(output));
        c.service.getFamiliesBiggerThan(4);
        assertLinesMatch(output.toString().lines(), ("""
                \tFamily = family1{\s
                \tparentA = parentA parentA,\s
                \tparentB = parentB parentB,\s
                \tchildren = [Human{\s
                \tname = 'child5',\s
                \tsurname = 'child5',\s
                \tyear = 01/01/1970,\s
                \tiq = 0,\s
                \tschedule = []
                }
                , Human{\s
                \tname = 'child6',\s
                \tsurname = 'child6',\s
                \tyear = 01/01/1970,\s
                \tiq = 0,\s
                \tschedule = []
                }
                , Human{\s
                \tname = 'child8',\s
                \tsurname = 'child8',\s
                \tyear = 01/01/1970,\s
                \tiq = 0,\s
                \tschedule = []
                }
                ],\s
                \tpet = []}

                """).lines());
        System.setOut(null);
    }
}
