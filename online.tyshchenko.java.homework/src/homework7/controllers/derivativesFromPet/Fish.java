package homework7.controllers.derivativesFromPet;

import homework7.baseClasses.Pet;
import homework7.enumerations.Species;
import homework7.views.HelpObj;
import homework7.views.OverrideToString;

import java.util.Set;

import static libs.Console.log;

public class Fish extends Pet {

    public final Species species;

    public Fish(String nickname) {
        super(nickname);
        this.species = Species.UNKNOWN;
    }

    public Fish(Species species, String nickname) {
        super(nickname);
        this.species = species;
    }

    public Fish(int age, int trickLevel, Set<String> habits, Species species, String nickname) {
        super(age, trickLevel, habits, nickname);
        this.species = species;
    }

    public Species getSpecies() {
        return species;
    }

    @Override
    public void eat() {
        log(String.format("%s кушает \n", this.getNickname()));
    }

    @Override
    public void respond() {
        log(String.format("%s - это рыба \n", this.getNickname()));
    }

    @Override
    public String toString() {
        return OverrideToString.overrideToStringPets(
                new HelpObj.HelpObjPet(
                        this.species, this.getNickname(), this.getAge(),
                        this.getTrickLevel(), this.getHabits(), this.species.canFly,
                        this.species.numberOfLegs, this.species.hasFur));
    }
}
