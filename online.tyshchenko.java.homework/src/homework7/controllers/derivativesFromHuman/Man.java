package homework7.controllers.derivativesFromHuman;

import homework7.baseClasses.Human;

import java.util.Map;

import static libs.Console.log;

public final class Man extends Human {
    public Man(int year, String... a) {
        super(year, a);
    }

    public Man(int year, int iq, Map<String, String> schedule, String... a) {
        super(year, iq, schedule, a);
    }

    public Man() {
    }

    public String repairCar() {
        return "чинит авто";
    }

    @Override
    public void greetPet() {
        log(String.format("Идем гулять %s?", this.getFamily().getPet().getNickname()));
    }
}
