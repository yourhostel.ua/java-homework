package homework7.baseClasses;

import homework7.views.HelpObj;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static homework7.views.OverrideToString.overrideToStringFamily;
import static libs.Console.err;
import static libs.Console.log;

public class Family {

    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";

    private String familyName;
    private Human parentA;
    private Human parentB;
    private final List<Human> children;
    private Pet pet;

    public Family(Human parentA, Human parentB, String familyName) {
        this.parentA = parentA;
        this.parentB = parentB;
        this.familyName = familyName;
        this.children = new ArrayList<>();
        this.pet = null;
        this.parentA.setFamily(this);
        this.parentB.setFamily(this);
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setParentA(Human parentA) {
        this.parentA = parentA;
    }

    public void setParentB(Human parentB) {
        this.parentB = parentB;
    }

    public void addChild(Human child) {
        child.setFamily(this);
        String massage = BLUE + String.format("ребенок %s %s добавлен в семью %s \n", child.getName(), child.getSurname(), this.familyName) + RESET;
        this.children.add(child);
        log(massage);
    }

    public void deleteChild(Human child) {
        if (this.children.size() == 0) {
            err(String.format("в семье %s детей нет\n", this.familyName));
            return;
        }
        if (!this.children.remove(child)) {
            err(String.format("ребенка %s %s в семье %s нет\n", child.getName(), child.getSurname(), this.familyName));
            return;
        }
        err(String.format("ребенок %s %s удален из семьи %s\n", child.getName(), child.getSurname(), this.familyName));
    }

    public boolean deleteChild(int index) {
        if (index < 0) {
            err(String.format("index = %d Индекс не может быть отрицательным\n", index));
            return false;
        }
        if (this.children.size() == 0) {
            err(String.format("в семье %s детей нет\n", this.familyName));
            return false;
        }
        Human human;
        try {
            human = this.children.remove(index);
        } catch (IndexOutOfBoundsException e) {
            err(String.format("в семье %s детей меньше чем задан индекс\n", this.familyName));
            return false;
        }
        err(String.format("ребенок %s %s удален из семьи %s\n", human.getName(), human.getSurname(), this.familyName));
        return true;
    }

    public Human getParentA() {
        return parentA;
    }

    public Human getParentB() {
        return parentB;
    }

    public List<Human> getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public int countFamily() {
        return this.children.size() + 2;
    }

    public void deleteFamily() {
        if (this.getChildren() != null) {
            for (Human child : this.getChildren()) {
                child.setFamily(null);//по можливості видаляємо всі посилання, щоб збирач сміття видалив об'єкт
            }
        }
        this.getParentA().setFamily(null);
        this.getParentB().setFamily(null);
        this.setParentA(null);
        this.setParentB(null);
        this.setPet(null);
        this.setFamilyName(null);
        log("семья удалена \n");
    }

    @Override
    public String toString() {
        String massage = "Был дан запрос о семье, но объект пуст, возможно данные были удалены";
        try {
            massage = overrideToStringFamily(new HelpObj.HelpObjFamily(this.familyName,
                    this.parentA.getName(), this.parentA.getSurname(),
                    this.parentB.getName(), this.parentB.getSurname(),
                    this.getChildren(), this.pet));
        } catch (Exception e) {
            err("Ошибка запроса данных о семье");
        }
        return massage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!Objects.equals(parentA, family.parentA)) return false;
        return Objects.equals(parentB, family.parentB);
    }

    @Override
    public int hashCode() {
        int result = parentA != null ? parentA.hashCode() : 0;
        result = 31 * result + (parentB != null ? parentB.hashCode() : 0);
        return result;
    }
}
